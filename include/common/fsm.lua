local m_weakRef = require 'common/weakref.lua'
local pairs = pairs
local ipairs = ipairs
local setmetatable = setmetatable
local assert = assert
local type = type
local tostring = tostring
local min = math.min
local debug = debug

-- internal utility function
local function ancestors(state)
    if not state.machine then
        return {state}
    end

    local res = {}
    local parent = state
    while parent do
        res[#res+1] = parent
        parent = parent.machine.parent.ref
    end
    
    return res
end

--[[
A Basic state
You can inherit from this class to implement OnEnter and OnExit actions
]]
local State = {}
State.__index = State

--[[
-- Creates a new state
@param name the name of the state
@param submachine the submachine of the state. The state assumes ownership of the submachine!
]]
function State:new(name, submachine)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.name = name
    obj.submachine = submachine
    
    if submachine then
        submachine.parent = m_weakRef.WeakRef(obj)
    end
    return obj
end

function State:SetSubmachine(sm)
    self.submachine = sm
    if sm then
        sm.parent = m_weakRef.WeakRef(self)
    end
end

function State:OnEnter()
end

function State:OnExit()
end

function State:OnTick()
end

function State:ToDot()
    local parent = nil
    if self.machine then
        parent = self.machine.parent.ref
    end
    if parent then
        return parent:ToDot() .. "_" .. self.name
    else
        return self.name
    end
end

function State:__tostring()
    local parent = nil
    if self.machine then
        parent = self.machine.parent.ref
    end
    if parent then
        return tostring(parent) .. "." .. self.name
    else
        return self.name
    end
end

-----------------------------------------

--[[
A finite state machine (FSM)
]]
local StateMachine = {}
StateMachine.__index = StateMachine

--[[
Creates a new StateMachine
@param states a table consisting of all states in the StateMachine. The StateMachine assumes ownership of all states !
@param start the starting state
@param tt the transition table
            each transition is constructed as follows: {state:from, string:event, state:to, [function:action], [function:condition]}
]]
function StateMachine:new(states, start, tt)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.states = {}
    obj.transitions = {}
    obj.start = start
    obj.parent = m_weakRef.WeakRef(nil)
    obj._events = {}
    
    for _, v in pairs(states) do
        obj:AddState(v)
    end
    
    for _, v in pairs(tt) do
        obj:AddTransition(v)
    end
    
    return obj
end

function StateMachine:OnStart(start, ...)
end

function StateMachine:OnStop()
end

--[[
Starts the StateMachine
Will enter the starting state, execute the OnEnter function of the starting state
and the enter all substates of the starting state
@param start the starting state (may be a state of a submachine or nil to use the default start state)
]]
function StateMachine:Start(start, ...)
    self:OnStart(start, ...)
    self.last = nil
    -- check if start is in current fsm
    local s = nil
    if start and self.states[start.name] == start then
        s = self.states[start.name]
    else
        s = self.start
    end
    
    -- enter start state
    if self.verbose then
        debug("Entering State " .. tostring(s))
    end
    self.current = s
    s:OnEnter(...)
    
    -- enter sub machines
    local sub = self.current.submachine
    if sub then
        sub:Start(start, ...)
    end
end

--[[
Stops the StateMachine
Will stop all submachines an then execute the OnExit function of the current state
]]
function StateMachine:Stop()
    local sub = self.current.submachine
    if sub then
        sub:Stop()
    end
    self.current:OnExit()
    self:OnStop()
    self.current = nil
end

--[[
Adds a state to the StateMachine
@param s the state to add. The StateMachine assumes ownership of the State!
]]
function StateMachine:AddState(state)
    assert(type(state) == "table")
    assert(type(state.name) == "string", "state.name is nil or not a string")
    assert(state.machine == nil, "state already belongs to a statemachine")
    assert(self.states[state.name] == nil, "there is already a state with the same name")
    
    self.states[state.name] = state
    state.machine = self
end


--[[
Adds a transition to the StateMachine
@param transition the transition to add
          format is {state:from, string:event, state:to, [function:condition]}
          
'from' state must be directly owned by this StateMachine
]]
function StateMachine:AddTransition(transition)
    local from = transition[1]
    local event = transition[2]
    local to = transition[3]
    local condition = transition.condition or transition[4]
    
    assert(from, "from state is nil")
    assert(type(event) == "string", "event is not a string")
    assert(self.states[from.name] == from, "from state doesnt (directly) belong to this statemachine")
    --assert(self:OwnsState(to), "to state doesnt belong to this statemachine")
    
    local transTable = self.transitions[from] or {}
    local list = transTable[event] or {}
    
    if not condition then
        for _, trans in ipairs(transTable) do
            assert(trans.condition, "There must only be one conditionless transition per event")
        end
    end
    
    table.insert(list, {to = to, condition = condition})

    self.transitions[from] = transTable
    transTable[event] = list
end

--[[
Returns true if the given state is owned by this StateMachine or by one of its children
]]
function StateMachine:OwnsState(state)
    local parents = ancestors(state)
    for _, state in ipairs(parents) do
        if state.machine == self then
            return true
        end
    end
    return false
end


function StateMachine:_Execute(event, ...)
    assert(type(event) == "string", "event must be a string")
    
    -- check if event is handled by submachine
    local sub = self.current.submachine
    if sub then
        if sub:_Execute(event, ...) then
            return true
        end
    end
    
    local transTable = self.transitions[self.current]
    if not transTable then
        return false
    end
    
    local transitions = transTable[event]
    if not transitions then return false end
    
    local elseTrans, toRun
    for _, trans in ipairs(transitions) do
        if not trans.condition then
            elseTrans = trans
        elseif trans.condition(self, ...) then
            toRun = trans
            break
        end
    end
    
    if not toRun then
        if not elseTrans then
            return false
        end
    
        toRun = elseTrans
    end
    
    -- find common ancestor
    local new = toRun.to
    
    local a = nil
    local aa = nil
    local ab = nil
    
    if new == self.current then
        a = new
    else
        aa = ancestors(self.current)
        ab = ancestors(new)
    
        for i = 0, min(#aa, #ab) - 1 do
            if aa[#aa - i] ~= ab[#ab - i] then
                break
            end
            a = ab[#ab - i - 1]
        end
    end
    
    if self.verbose then
        debug("Transition: " .. tostring(self.current) .. " - " .. event .. " -> " .. tostring(toRun.to))
    end
    
    -- exit all states up to the common ancestor
    local toexit = a or aa[#aa]
    local sub = toexit.submachine
    if sub then
        sub:Stop()
    end
    toexit:OnExit()
    
    -- enter all states starting from common ancestor
    local toenter = a or ab[#ab]
    if toenter.machine.verbose then
        debug("Entering State " .. tostring(toenter))
    end
    toenter.machine.current = toenter
    toenter:OnEnter(...)
    
    local sub = toenter.submachine
    if sub then
        sub:Start(new, ...)
    end

    return true
end


--[[
Executes a single step of the FSM
]]
function StateMachine:Step()
    -- run submachine if available
    local sub = self.current.submachine
    if sub then
        return sub:Step()
    end

    -- run current state
    self.current.OnTick()

    local e = table.remove(self._events, 1)
    self:_Execute(e.event, table.unpack(e.args))
end

--[[
Fires the event
@param event the name of the event to fire
@param ... parameters for the event
]]
function StateMachine:Fire(event, ...)
  table.insert(self._events, {event = event, args = {...}}
end

function StateMachine:ToDot()
    local res = ""
    
    if not self.parent.ref then
        res = res .. "digraph{\n"
        res = res .. "node [shape=Mrecord]\n"
        res = res .. "rankdir=lr;\n"
    end
    
    for name, state in pairs(self.states) do
        if state.submachine == nil then
            res = res .. state:ToDot() .. "[label=\"" .. name .. "\"];\n"
        else
            res = res .. state:ToDot() .. "[label=\"{<f0>" .. name .. "|<f1> \\<\\<submachine\\>\\>}\"];\n"
            
            res = res .. "subgraph cluster_" .. state:ToDot() .. "{\n"
            res = res .. "label = \"submachine " .. name .. "\";\n"
            res = res .. state:ToDot() .. "__START__" .. " [shape=point];\n"
            res = res .. state.submachine:ToDot()
            res = res .. "}\n"
            
            res = res .. state:ToDot() .. " -> " .. state:ToDot() .. "__START__ [style = dotted, arrowhead = none]\n"
            res = res .. state:ToDot() .. "__START__ -> " .. state.submachine.start:ToDot() .. "\n"
        end
    end
    for from, transTable in pairs(self.transitions) do
        for event, list in pairs(transTable) do
            for _, trans in ipairs(list) do
                if type(event) == "table" then event = "" end
                res = res .. from:ToDot() .. " -> " .. trans.to:ToDot() .. " [label = \"" .. event .. "\"]\n"
            end
        end
    end
    if not self.parent.ref then
        res = res .. "__START__ [shape=point];\n"
        res = res .. "__START__ ->" .. self.start:ToDot() .. "\n"
        res = res .. "}\n"
    end
    return res
end

function StateMachine:__tostring()
    return "StateMachine"
end

return {StateMachine = StateMachine, State = State}