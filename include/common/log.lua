local function FormatMsg(msg)
	return Client:GetEmail() .. ": " .. msg
end

local function ClientTrace(msg)
	trace(FormatMsg(msg))
end

local function ClientDebug(msg)
	debug(FormatMsg(msg))
end

local function ClientInformation(msg)
	information(FormatMsg(msg))
end

local function ClientNotice(msg)
	notice(FormatMsg(msg))
end

local function ClientWarning(msg)
	warning(FormatMsg(msg))
end

local function ClientError(msg)
	error(FormatMsg(msg))
end

local function ClientCritical(msg)
	critical(FormatMsg(msg))
end

local function ClientFatal(msg)
	fatal(FormatMsg(msg))
end

return {
	trace = ClientTrace,
	debug = ClientDebug,
	information = ClientInformation,
	info = ClientInformation,
	notice = ClientNotice,
	warning = ClientWarning,
	error = ClientError,
	critical = ClientCritical,
	fatal = ClientFatal
}