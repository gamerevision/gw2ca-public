--[[
RNG related functions.
This module automatically calls math.randomseed() on its very first import
]]

local m_util = include("gw2/util.lua")

if not __M_RAND_SEEDED then
    math.randomseed(time())
    __M_RAND_SEEDED = true
end

__M_RAND_DEFAULT_RNG = math.random
local _DefaultRNG = nil

local function GetRNG(RNG)
    if RNG then
        return RNG
    elseif _DefaultRNG then
        return _DefaultRNG
    else
        return __M_RAND_DEFAULT_RNG
    end
end

--[[
Sets the RNG to be used as default for all methods in this module.
This method has global impact and replaces the default RNG for all instances of this module!

If global is set to false, only the default RNG of current module will be replaced.
]]
local function SetDefaultRNG(RNG, global)
    if not global or global == true then
        __M_RAND_DEFAULT_RNG = RNG
    else
        _DefaultRNG = RNF
    end
end

--[[
Round half to even
]]
local function Round(real)
    assert(type(real) == "number", "real must not be nil and a number")

    local decimal = real % 1
    local lower = math.floor(real)
    local upper = math.ceil(real)
    
    if decimal < 0.5 then
        return lower
    elseif decimal > 0.5 then
        return upper
    elseif lower % 2 == 0 then
        return lower
    else
        return upper
    end
end

--[[
Behaves exactly like Random() except that the bounded versions return floats instead of integers
]]
local function RandomFloat(lower, upper, RNG)
    RNG = GetRNG(RNG)
    
    if lower == nil then
        -- real number between 0 and 1
        return RNG()
    elseif upper == nil then
        -- integer number between 1 and lower
        return 1 + RNG() * (lower - 1)
    else
        -- integer number between lower and upper
        return lower + RNG()* (upper - lower)
    end
end

--[[
Behaves exactly the same like math.random excecpt that this function uses the set default RNG
]]
local function Random(lower, upper, RNG)
    RNG = GetRNG(RNG)
     
    if lower == nil then
        -- real number between 0 and 1
        return RNG()  
    else
        return Round(RandomFloat(lower, upper, RNG))
    end
end

--[[
Returns a new random (float) number distributed in the relative margin around the given center.

Example: 
    RndDistribute(100, 0.1) -- returns values between 90 and 110
]]
local function RandDistribute(center, margin, RNG)
    assert(type(center) == "number", "center must not be nil and a number")
    assert(type(margin) == "number", "margin must not be nil and a number")

    RNG = GetRNG(RNG)

    local absMargin = center * margin
    return center - absMargin + absMargin * 2 * RNG()
end

--[[
Returns a new random (float) number distributed in the absolute margin around the given center.

Example:
    RndDistributeAbs(100, 25) -- returns values between 75 and 125
]]
local function RandDistributeAbs(center, margin, RNG)
    assert(type(center) == "number", "center must not be nil and a number")
    assert(type(margin) == "number", "margin must not be nil and a number")
    
    RNG = GetRNG(RNG)

    return center - margin + margin * 2 * RNG()
end

--[[
Takes in an array and returns a randomly shuffled copy of it using the Fisher-Yate algorithm.
Due to performance reason this function returns a different result that the ShuffleInplace() method but is still O(n).
]]
local function Shuffle(array, RNG)
    assert(array ~= nil, "array must not be nil")

    local length = #array
    local copy = {}
    for i=1, #array, 1 do
        local rand = Random(1, length, RNG)
        copy[i] = copy[rand]
        copy[rand] = array[i]
    end
    
    return copy
end

--[[
Shuffles the given array in place using the Fisher-Yate algorithm having a time-complexity of O(n)
]]
local function ShuffleInplace(array, RNG)
    assert(array ~= nil, "array must not be nil")

    local length = #array
    for i=#array, 2, -1 do
        local rand = Random(1, length, RNG)
        
        local temp = array[i]
        array[i] = array[rand]
        array[rand] = temp
    end
    
    return array
end

local function ShuffledPairsIterator(invariant, oldKey)
    local index
    if oldKey ~= nil then
        for i, k in ipairs(invariant.keys) do
            if k == oldKey then
                index = i
            end
        end
        
        if index == nil then
            return nil
        end
    end

    local i, key = next(invariant.keys, index)
    if i ~= nil then
        return key, invariant.tbl[key]
    end
end

--[[
Identical to pairs() except that the table is always traversed in a different random order. 

As tables are unordered by definition, we can't shuffle them.
However we can iterate over them.
]]
local function ShuffledPairs(tbl, RNG)
    assert(tbl ~= nil, "tbl must not be nil")

    local invariant = {}
    invariant.keys = ShuffleInplace(m_util.GetKeys(tbl), RNG)
    invariant.tbl = tbl
    
    return ShuffledPairsIterator, invariant, nil
end

return {
    SetDefaultRNG = SetDefaultRNG,
    Round = Round,
    Random = Random,
    RandomFloat = RandomFloat,
    RandDistribute = RandDistribute,
    RandDistributeAbs = RandDistributeAbs,
    Shuffle = Shuffle,
    ShuffleInplace = ShuffleInplace,
    ShuffledPairs = ShuffledPairs
}