--[[
Creates a weak reference to the specified value.
@param v the value to reference

Note that this will only work for tables and userdata.
Also note that in order to do comparisions you will have need to use .ref since lua
will only call comparision operators iff both tables have the same metamethod for the requested operator.
]]
local mt = {
    __mode = 'v',
    __index = function(self, k) if not rawget(self, "ref") then return nil else return self.ref[k] end end,
    __newindex = function(self, k, v) assert(rawget(self, "ref"), "Tried to index a nil value"); self.ref[k] = v end,
}

local function WeakRef(v)
    local ref = setmetatable({ref = v}, mt)
    return ref
end

return {WeakRef = WeakRef}