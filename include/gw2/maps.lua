local m_util = include("gw2/util.lua")

local export = {}

--[[
A class holding informations about a waypoint.
]]
local Waypoint = {}
Waypoint.__index = Waypoint

function Waypoint:new(id, mapId, pos)
    local obj = setmetatable({}, self)
    obj.__index = obj

    assert(id ~= nil, "id must not be nil")
    assert(mapId ~= nil, "mapId must not be nil")
    assert(pos ~= nil, "pos must not be nil")

    obj.id = id
    obj.mapId = mapId
    obj.pos = pos

    return obj
end

function Waypoint:GetPos()
    return self.pos
end

function Waypoint:SetPos(pos)
    if pos == nil then
        error("Waypoint.SetPos() - pos must not be nil")
        return
    end

    self.pos = pos
end


--[[
A class holding informations about a single map, e.g name and connecting neighbors.
]]
local Map = {}
Map.__index = Map

function Map:new(id, name, minLevel, transitions)
	local obj = setmetatable({}, self)
    obj.__index = obj

	obj.id = id
	obj.name = name

	if minLevel ~= nil then
		obj.minLevel = minLevel
	else
		obj.minLevel = 0
	end

	if transitions ~= nil then
		obj.transitions = transitions
	else
		obj.transitions = {}
	end

    obj.waypoints = {}

	return obj
end

--[[
Adds a transition from this to another map.
Notice that this transition is directed, that means this function has to be called
on both map objects.
]]
function Map:AddTransition(toId, startPos, endPos)
    for i, trans in pairs(self.transitions) do
        if trans.toId == toId and m_util.InRange(startPos, trans.startPos, 1000) then
            error("There is already a transition from " .. self.id .. " to " .. toId .. " (in close proximity)")
            return false
        end
    end

     table.insert(self.transitions, { toId = toId, startPos = startPos, endPos = endPos})
     return true
end

--[[
Returns whether there is a transition from this map to another map with the given id.
]]
function Map:HasTransition(toId)
	for i, trans in pairs(self.transitions) do
		if trans.toId == toId then
			return true
		end
	end

	return false
end

--[[
Returns all transitions from this map to another map with the given id.
]]
function Map:GetTransitions(toId)
	local arr = {}
	for i, trans in ipairs(self.transitions) do
		if trans.toId == toId then
			table.insert(arr, trans)
		end
	end

	return arr
end

--[[
Adds a waypoint to this map
]]
function Map:AddWaypoint(wpId, pos)
    self.waypoints[wpId] = Waypoint:new(wpId, self.id, pos)
end

--[[
Returns whether the waypoint with the given id is contained on this map
]]
function Map:HasWaypoint(wpId)
    return self.waypoints[wpId] ~= nil
end

--[[
Returns the waypoint with the given wpId or nil if such a waypoint doesn't exist on this map
]]
function Map:GetWaypoint(wpId)
    return self.waypoints[wpId]
end

--[[
Returns all waypoints of the current map
]]
function Map:GetWaypoints()
    return self.waypoints
end

--[[
Returns the closest unlocked waypoint to the given pos on this map.
If no waypoint could be found (either because we lack data or no waypoint is unlocked) this function returns nil.
]]
function Map:GetClosestWaypoint(client, pos)
    local client = m_util.GetClient(client)

    assert(pos ~= nil, "pos must not be nil")

    local best = nil
    local smallest = 0
    for wpId, wp in pairs(self.waypoints) do
        if not client:GetPoiMgr():IsLocked(wpId) and not client:GetPoiMgr():IsContested(wpId) then
            local dist = m_util.ComputeDistance(wp.pos, pos)
            if dist < smallest or best == nil then
                smallest = dist
                best = wp
            end
        end
    end

    return best
end

--[[
A table of maps which provides additional functionality like
integrity validation and A* pathfinding
]]
local MapTable = {}
MapTable.__index = MapTable

function MapTable:new()
	local obj = setmetatable({}, self)

	-- operator[] overload:
	obj.__index = function(this, key)
		local map = MapTable.GetMap(this, key)
		if map ~= nil then
			return map
		else
			return getmetatable(this)[key]
		end
	end
	obj = setmetatable({}, obj)
    obj.__index = obj

	obj.maps = {}

	return obj
end

--[[
Adds a map to this MapTable
--]]
function MapTable:AddMap(map)
	if self:HasMap(map.id) then
		error("MapTable.AddMap() - There is already a map with id " .. map.id)
		return
	end

	self.maps[map.id] = map
end

--[[
Returns the map with the given id
]]
function MapTable:GetMap(id)
	return self.maps[id]
end

--[[
Returns whether this table contains a map with the given id
]]
function MapTable:HasMap(id)
	if id == nil then
		error("MapTable.HasMap() - passed id is nil")
		return false
	end

	return self:GetMap(id) ~= nil
end

--[[
Returns the map on which the given client is currently logged in or nil if the map is not contained in this MapTable
]]
function MapTable:GetCurrentMap(client)
    client = m_util.GetClient(client)
    return self:GetMap(client:GetMapId())
end

--[[
Returns the underlying array
]]
function MapTable:GetAll()
	return self.maps
end

--[[
Returns the name of the map with the given id.
If there isn't a map with such an id, this function returns "UNK_MAP_{id}"
]]
function MapTable:GetName(id)
	if not self:HasMap(id) then
		return "UNK_MAP_" .. id
	end

	return self:GetMap(id).name
end

--[[
Removes all transitions of the map with the given id. Removes both directions.
]]
function MapTable:RemoveTransitions(id)
    local map = self:GetMap(id)

    if not map then
        error("No map with this id")
        return
    end

    for i, trans in ipairs(map.transitions) do
        local toMap = self:GetMap(trans.toId)

        if not toMap then
            error("Corrupted map data, call CheckIntegrity() for a detailed error message")
            return
        end

        local toRemove = {}
        for j, toTrans in ipairs(toMap.transitions) do
            if toTrans.toId == id then
                toRemove[j] = true
            end
        end

        for index in pairs(toRemove) do
            table.remove(toMap.transitions, index)
        end
    end

    map.transitions = {}
end

--[[
This function checks whether the table is integer.
In particular this function checks for any hanging transitions and mismatchijng ids.
If anything is ok this function returns nil otherwise it returns a string describing the error.
]]
function MapTable:CheckIntegrity()
	for id, map in pairs(self.maps) do
		-- check transitions
		for i, trans in pairs(map.transitions) do
			local targetMap = self:GetMap(trans.toId)
			if targetMap == nil then
				return string.format("Transition from %d (%s) to %u but %u is not registered", map.id, map.name, trans.toId, trans.toId)
			elseif not targetMap:HasTransition(map.id) then
				return string.format("Transition from %d (%s) to %u (%s) but no transition in the opposite direction", map.id, map.name, targetMap.id, targetMap.name)
			end
		end

		-- check for mismatching id
		if id ~= map.id then
			return "Mismatching ids: " .. id .. " and " .. map.id
		end
	end

	return nil
end

--[[
Computes all pairs shortest path.
@param filter a table containing all map ids to ignore in the calculation
@return a 2d array containing all paths in the form:
            [fromid][toid] = {map1id, map2id, map3id, ..., toid }
]]
function MapTable:ComputeAPSP(filter)
    if filter == nil then
        filter = function(x) return true end
    end

    local dist = {}
    local next_ = {}

    local maps = {}
    for id, map in pairs(self:GetAll()) do
        if filter(map) then
            table.insert(maps, map)
        end
    end

    -- init arrays
    for _, from in ipairs(maps) do
        dist[from.id] = {}
        next_[from.id] = {}
        for _, to in ipairs(maps) do
            if from == to then
                dist[from.id][to.id] = 0
                next_[from.id][to.id] = to.id
            elseif from:HasTransition(to.id) then
                dist[from.id][to.id] = 1
                next_[from.id][to.id] = to.id
            else
                dist[from.id][to.id] = #maps * #maps
            end
        end
    end

    -- floyd warshall
    for _, k in ipairs(maps) do
        for _, i in ipairs(maps) do
            for _, j in ipairs(maps) do
                if dist[i.id][k.id] + dist[k.id][j.id] < dist[i.id][j.id] then
                    dist[i.id][j.id] = dist[i.id][k.id] + dist[k.id][j.id]
                    next_[i.id][j.id] = next_[i.id][k.id]
                end
            end
        end
    end

    -- compute path from u to v
    local function ComputePath(u, v)
        if next_[u] == nil or next_[u][v] == nil then
            return nil
        end

        local path = {}
        while u ~= v do
            u = next_[u][v]
            table.insert(path, u)
        end
        return path
    end

    -- compute all paths
    local result = {}
    for id, from in pairs(self:GetAll()) do
        result[from.id] = {}
        for id, to in pairs(self:GetAll()) do
            result[from.id][to.id] = ComputePath(from.id, to.id)
        end
    end

    return result
end

local mapTable = MapTable:new()
local vars = {}

-------------------------------------------------------
--                    STATIC DATA                    --
-------------------------------------------------------

vars.Queensdale = Map:new(15, "Queensdale", 1)
vars.Queensdale:AddTransition(23, WorldPos(-28114.5, -24101.4, -1367.85), WorldPos(-26162, 21618.7, -3238.83))
vars.Queensdale:AddTransition(18, WorldPos(-15052.2, 23859.2, -2981.59), WorldPos(2403.31, -20219, -3945.52))
vars.Queensdale:AddTransition(24, WorldPos(40241.3, 7587.6, -1268.33), WorldPos(-46827.6, 14922.7, -255.935))
vars.Queensdale:AddTransition(23, WorldPos(14018.9, -26463.2, -249.043), WorldPos(20785.3, 22599.5, -2083.2))
vars.Queensdale:AddWaypoint(240, WorldPos(-25418, 14188, -870))
vars.Queensdale:AddWaypoint(241, WorldPos(-7707, 12200, -1122))
vars.Queensdale:AddWaypoint(242, WorldPos(-19961.3, -4961.67, -492.211))
vars.Queensdale:AddWaypoint(243, WorldPos(3187.57, -11867.8, -1140.1))
vars.Queensdale:AddWaypoint(244, WorldPos(-34072, -9213, -822))
vars.Queensdale:AddWaypoint(245, WorldPos(-4238, -18270, -1358))
vars.Queensdale:AddWaypoint(246, WorldPos(-22417.1, -15853, -1374.34))
vars.Queensdale:AddWaypoint(247, WorldPos(13042, -24583, -269))
vars.Queensdale:AddWaypoint(248, WorldPos(17628, 5560, -1433))
vars.Queensdale:AddWaypoint(249, WorldPos(7170, 16685, -35))
vars.Queensdale:AddWaypoint(250, WorldPos(37238, 21420, -840))
vars.Queensdale:AddWaypoint(251, WorldPos(30590, 3980, -1175))
vars.Queensdale:AddWaypoint(252, WorldPos(20608, -10633, -84))
vars.Queensdale:AddWaypoint(837, WorldPos(-6497.08, 2839.58, -116.549))
vars.Queensdale:AddWaypoint(836, WorldPos(-37251, 19257.5, -578.832))
vars.Queensdale:AddWaypoint(239, WorldPos(-16502, 17330, -1354))

vars.Hinterlands = Map:new(17, "Harathi Hinterlands", 35)
vars.Hinterlands:AddTransition(24, WorldPos(18572.6, -31569.4, -1365.51), WorldPos(5531.51, 22427.6, -790.411))
vars.Hinterlands:AddTransition(24, WorldPos(-15247.4, -30239.5, -3862.79), WorldPos(-27467, 23041.4, -2172.21))
vars.Hinterlands:AddWaypoint(176, WorldPos(6906.13, 20402.9, -2510.55))
vars.Hinterlands:AddWaypoint(177, WorldPos(25567.3, 26140.1, -4361.93))
vars.Hinterlands:AddWaypoint(178, WorldPos(-29888, 26961.5, -1511.99))
vars.Hinterlands:AddWaypoint(195, WorldPos(-4435, 26991, -2505))
vars.Hinterlands:AddWaypoint(165, WorldPos(-13685, -23277, -3132.99))
vars.Hinterlands:AddWaypoint(166, WorldPos(17888.3, -30032.8, -1126.23))
vars.Hinterlands:AddWaypoint(167, WorldPos(-28179.1, 16028.5, -680.354))
vars.Hinterlands:AddWaypoint(168, WorldPos(-20307.6, -2470.16, -1522.05))
vars.Hinterlands:AddWaypoint(169, WorldPos(31298.9, -16622.6, -1807.35))
vars.Hinterlands:AddWaypoint(170, WorldPos(-2805, -29593, -7))
vars.Hinterlands:AddWaypoint(171, WorldPos(-29707.7, -8992.47, -3038.68))
vars.Hinterlands:AddWaypoint(172, WorldPos(2847.53, -4589.88, -169.477))
vars.Hinterlands:AddWaypoint(173, WorldPos(19194.9, -842.065, -1683.73))
vars.Hinterlands:AddWaypoint(174, WorldPos(847, 11086, -1752.88))
vars.Hinterlands:AddWaypoint(175, WorldPos(-8638, 15298, -1335))

vars.DivinitysReach = Map:new(18, "Divinity's Reach")
vars.DivinitysReach:AddTransition(15, WorldPos(2633.62, -20532.9, -3947.42), WorldPos(-15004.2, 23312.2, -2874.72))
vars.DivinitysReach:AddTransition(21, WorldPos(19256.9, -5169.91, -2648.82), WorldPos(-27239.6, -15022.8, -2724.47))
vars.DivinitysReach:AddTransition(50, WorldPos(4921.02, -6089.64, -5722.99), WorldPos(5073.1, 10256.6, -1545))
vars.DivinitysReach:AddWaypoint(803, WorldPos(2199, -19631, -3927))
vars.DivinitysReach:AddWaypoint(804, WorldPos(3347, 15291, -3984))
vars.DivinitysReach:AddWaypoint(805, WorldPos(17609, -11732, -3961.42))
vars.DivinitysReach:AddWaypoint(806, WorldPos(18153, 6025, -3913))
vars.DivinitysReach:AddWaypoint(807, WorldPos(-12664, -10438, -3948))
vars.DivinitysReach:AddWaypoint(808, WorldPos(-12349, 6856, -3912))
vars.DivinitysReach:AddWaypoint(809, WorldPos(3167.82, -1375.33, -5386.1))
vars.DivinitysReach:AddWaypoint(810, WorldPos(319.948, -10755.4, -3135.37))
vars.DivinitysReach:AddWaypoint(811, WorldPos(12155, -2475, -3132))
vars.DivinitysReach:AddWaypoint(812, WorldPos(-6617.38, -2245.69, -2836.3))
vars.DivinitysReach:AddWaypoint(813, WorldPos(-2043, 6477, -3136))
vars.DivinitysReach:AddWaypoint(814, WorldPos(8697.82, 4983.51, -3139.24))
vars.DivinitysReach:AddWaypoint(1278, WorldPos(3455.16, -10428.7, -5463.77))

vars.Ashford = Map:new(19, "Plains of Ashford", 1)
vars.Ashford:AddTransition(218, WorldPos(-44759, 4313.96, -2620.88), WorldPos(16259.7, 8498.41, -5180.26))
vars.Ashford:AddTransition(25, WorldPos(25937.3, 22793.1, -1551.08), WorldPos(634.198, -46885, -1081.43))
vars.Ashford:AddTransition(32, WorldPos(-10002.5, 22356.9, -2605.94), WorldPos(35681.2, -26055.8, -484.179))
vars.Ashford:AddTransition(20, WorldPos(47262.5, -6035.62, -1696.74), WorldPos(-21768.8, -16355, -1430.02))
vars.Ashford:AddWaypoint(384, WorldPos(-42373.1, 9269.2, -2033.68))
vars.Ashford:AddWaypoint(385, WorldPos(-42771.5, 19267.4, -2238.78))
vars.Ashford:AddWaypoint(386, WorldPos(-8223, 18199, -2282))
vars.Ashford:AddWaypoint(387, WorldPos(-17843, 1110, -2078))
vars.Ashford:AddWaypoint(388, WorldPos(-1987.21, -10344.6, -1730.77))
vars.Ashford:AddWaypoint(389, WorldPos(-12891, -12372, -311))
vars.Ashford:AddWaypoint(390, WorldPos(12695.3, 13037.6, -1367.02))
vars.Ashford:AddWaypoint(967, WorldPos(30184, -17804, -108))
vars.Ashford:AddWaypoint(392, WorldPos(38621.3, 3767.36, -1358.97))
vars.Ashford:AddWaypoint(393, WorldPos(42961.4, -6991.3, -1327.88))
vars.Ashford:AddWaypoint(394, WorldPos(22944, -8788, -108))
vars.Ashford:AddWaypoint(919, WorldPos(-25417.1, 8964.7, -1597.24))
vars.Ashford:AddWaypoint(920, WorldPos(-34110, -13674, -1065))
vars.Ashford:AddWaypoint(921, WorldPos(-186.388, 6176.6, -1984.36))
vars.Ashford:AddWaypoint(1784, WorldPos(-32435.5, -20110.3, -2022.86))
vars.Ashford:AddWaypoint(968, WorldPos(24454, 6915, -498))
vars.Ashford:AddWaypoint(391, WorldPos(25997.5, 19769, -1451.52))
vars.Ashford:AddWaypoint(383, WorldPos(-34135, -1335, -1822))

vars.Blazeridge = Map:new(20, "Blazeridge Steppes", 40)
vars.Blazeridge:AddTransition(25, WorldPos(-21438, 38223.6, -739.278), WorldPos(22054.2, -22167.7, -444.101))
vars.Blazeridge:AddTransition(19, WorldPos(-22567.7, -16238.9, -1414.63), WorldPos(46716.9, -5968.55, -1614.02))
vars.Blazeridge:AddTransition(21, WorldPos(17280.3, -47340.8, -1748.6), WorldPos(17149, 32028.8, -96.4061))
vars.Blazeridge:AddWaypoint(512, WorldPos(17322, 19751, -1468))
vars.Blazeridge:AddWaypoint(513, WorldPos(12119, 35452, -811))
vars.Blazeridge:AddWaypoint(514, WorldPos(-4800, 38056, -709))
vars.Blazeridge:AddWaypoint(515, WorldPos(-20352, 39148, -772))
vars.Blazeridge:AddWaypoint(516, WorldPos(-18085, 16124, -1396))
vars.Blazeridge:AddWaypoint(517, WorldPos(-4341, -3831, -1057))
vars.Blazeridge:AddWaypoint(846, WorldPos(-9230, 31770, -560))
vars.Blazeridge:AddWaypoint(847, WorldPos(13367, -4340, -2835))
vars.Blazeridge:AddWaypoint(848, WorldPos(-14352, -1652, -1562))
vars.Blazeridge:AddWaypoint(849, WorldPos(8851.22, -36111.4, -569.515))
vars.Blazeridge:AddWaypoint(850, WorldPos(-2480, -29096, -733))
vars.Blazeridge:AddWaypoint(505, WorldPos(-19817, -17783, -1280))
vars.Blazeridge:AddWaypoint(506, WorldPos(-11173, -24398, -224))
vars.Blazeridge:AddWaypoint(507, WorldPos(-11293, -38842, -843.86))
vars.Blazeridge:AddWaypoint(508, WorldPos(15706.9, -45592.7, -1119.2))
vars.Blazeridge:AddWaypoint(509, WorldPos(10012, -23160, -3013))
vars.Blazeridge:AddWaypoint(510, WorldPos(13000, 9966, -989))
vars.Blazeridge:AddWaypoint(511, WorldPos(-1685, 18153, -899))

vars.FieldsOfRuin = Map:new(21, "Fields of Ruin", 30)
vars.FieldsOfRuin:AddTransition(20, WorldPos(17378.4, 32753.2, -128.755), WorldPos(17123.6, -46935.3, -1673.53))
vars.FieldsOfRuin:AddTransition(18, WorldPos(-27426.6, -15042.6, -2723.26), WorldPos(19135.5, -5055.42, -2596.6))
vars.FieldsOfRuin:AddWaypoint(330, WorldPos(-8221.03, 18835.5, -394.83))
vars.FieldsOfRuin:AddWaypoint(331, WorldPos(-29552, 11948, -575))
vars.FieldsOfRuin:AddWaypoint(332, WorldPos(21370, 19235, -353))
vars.FieldsOfRuin:AddWaypoint(333, WorldPos(-9881, 4863, -643))
vars.FieldsOfRuin:AddWaypoint(334, WorldPos(3285, -6105, -648))
vars.FieldsOfRuin:AddWaypoint(335, WorldPos(16280, -18190, -1387))
vars.FieldsOfRuin:AddWaypoint(336, WorldPos(-795, -27892, -693))
vars.FieldsOfRuin:AddWaypoint(337, WorldPos(-28637, -26055, -2375))
vars.FieldsOfRuin:AddWaypoint(211, WorldPos(-20448, -14599, -2394))
vars.FieldsOfRuin:AddWaypoint(212, WorldPos(26505, -7695, -1243))
vars.FieldsOfRuin:AddWaypoint(213, WorldPos(3069, 10749, -977))
vars.FieldsOfRuin:AddWaypoint(214, WorldPos(-28406, -5522, -413))
vars.FieldsOfRuin:AddWaypoint(215, WorldPos(-21667.6, 28334.5, -570.108))
vars.FieldsOfRuin:AddWaypoint(216, WorldPos(14269.8, 29246, -127.909))
vars.FieldsOfRuin:AddWaypoint(1084, WorldPos(-4661, -13278, -1239))
vars.FieldsOfRuin:AddWaypoint(1085, WorldPos(-14675, -24195, -2748))
vars.FieldsOfRuin:AddWaypoint(1086, WorldPos(-9203, 28355, -812))

vars.Fireheart = Map:new(22, "Fireheart Rise", 60)
vars.Fireheart:AddTransition(25, WorldPos(38088.2, -21378.4, -319.238), WorldPos(-21989.4, 17906.6, -961.07))
vars.Fireheart:AddTransition(30, WorldPos(-35503.5, 2792.69, -3539.93), WorldPos(34359.8, 1545.11, -661.808))
vars.Fireheart:AddWaypoint(544, WorldPos(-26864, 25789, -1946))
vars.Fireheart:AddWaypoint(545, WorldPos(-16858, 31893, -3147))
vars.Fireheart:AddWaypoint(546, WorldPos(-6372, 22579, -2510))
vars.Fireheart:AddWaypoint(547, WorldPos(6191.47, 28611.5, -1374.14))
vars.Fireheart:AddWaypoint(548, WorldPos(8260, 20539, -1067))
vars.Fireheart:AddWaypoint(549, WorldPos(17244.1, 13585.9, -740.166))
vars.Fireheart:AddWaypoint(550, WorldPos(851, 8847, -1744))
vars.Fireheart:AddWaypoint(1344, WorldPos(34618.4, 26129.3, -2313.43))
vars.Fireheart:AddWaypoint(534, WorldPos(36116.3, -22138.9, -282.998))
vars.Fireheart:AddWaypoint(535, WorldPos(17470.4, -16768.1, -248.305))
vars.Fireheart:AddWaypoint(536, WorldPos(27484.1, -26877.4, -73.807))
vars.Fireheart:AddWaypoint(537, WorldPos(9347.89, -26316.3, -57.6736))
vars.Fireheart:AddWaypoint(538, WorldPos(31898.9, 1146.15, -691.688))
vars.Fireheart:AddWaypoint(539, WorldPos(-8265.63, -11499.3, -841.845))
vars.Fireheart:AddWaypoint(540, WorldPos(-4447.4, -23451.4, -1559.71))
vars.Fireheart:AddWaypoint(541, WorldPos(-25885.2, -18209.1, -1740.43))
vars.Fireheart:AddWaypoint(542, WorldPos(-34021.6, 1518.4, -3243.54))
vars.Fireheart:AddWaypoint(543, WorldPos(-14118.1, 1535.93, -1071.35))

vars.Kessex = Map:new(23, "Kessex Hills", 15)
vars.Kessex:AddTransition(15, WorldPos(-26179.4, 22127, -3245.37), WorldPos(-27657.5, -23863.1, -1362.59))
vars.Kessex:AddTransition(34, WorldPos(-7222.57, -22105.7, -1833.72), WorldPos(19232.3, 47474.6, -655.309))
vars.Kessex:AddTransition(54, WorldPos(-45687.2, 11755.5, -3034.67), WorldPos(39707.3, 28667.4, -2646.6))
vars.Kessex:AddTransition(15, WorldPos(20252, 23032.8, -2258.05), WorldPos(13878.1, -25878.9, -229.841))
vars.Kessex:AddTransition(24, WorldPos(47063.7, 20954.1, -837.281), WorldPos(-45795.4, -19046.9, -343.876))
vars.Kessex:AddWaypoint(3, WorldPos(-35158.1, 19378.9, -3703.32))
vars.Kessex:AddWaypoint(4, WorldPos(-33612, -4589, -927.666))
vars.Kessex:AddWaypoint(6, WorldPos(-44647, 11051.8, -2923.48))
vars.Kessex:AddWaypoint(7, WorldPos(24077, 19776, -1509))
vars.Kessex:AddWaypoint(8, WorldPos(38565.1, 16522.3, -955.835))
vars.Kessex:AddWaypoint(16, WorldPos(-7826, 1647, -50.5619))
vars.Kessex:AddWaypoint(17, WorldPos(44136.7, -2198.29, -1458.39))
vars.Kessex:AddWaypoint(18, WorldPos(32839.3, -11101.6, -3203.06))
vars.Kessex:AddWaypoint(19, WorldPos(13989, -13531, -2655))
vars.Kessex:AddWaypoint(20, WorldPos(16390.9, 2569.44, -2003.63))
vars.Kessex:AddWaypoint(21, WorldPos(9585.13, 20506, -3072.03))
vars.Kessex:AddWaypoint(22, WorldPos(-13700.7, 19971.6, -2514.01))
vars.Kessex:AddWaypoint(953, WorldPos(30384, 7847, -1785))
vars.Kessex:AddWaypoint(10, WorldPos(-14936.3, -16221.7, -132.135))
vars.Kessex:AddWaypoint(12, WorldPos(-31655.9, -20212.3, -34.3262))
vars.Kessex:AddWaypoint(954, WorldPos(2347.12, 15064.3, -1513.81))

vars.Gendarran = Map:new(24, "Gendarran Fields", 25)
vars.Gendarran:AddTransition(15, WorldPos(-47095.3, 15060, -222.343), WorldPos(39683.8, 7887.09, -1080.55))
vars.Gendarran:AddTransition(23, WorldPos(-46165.9, -19140.2, -357.711), WorldPos(46589, 20885.6, -789.243))
vars.Gendarran:AddTransition(17, WorldPos(5569.79, 22810.4, -803.03), WorldPos(18328.4, -31193.5, -1340.62))
vars.Gendarran:AddTransition(17, WorldPos(-27441, 23424.8, -2218.93), WorldPos(-14860, -29886.6, -3741.9))
vars.Gendarran:AddTransition(31, WorldPos(49380.1, 13513.4, -3286.13), WorldPos(-46006, -10580.4, -1486.54))
vars.Gendarran:AddTransition(27, WorldPos(47992.5, -7678.02, -3442.13), WorldPos(-18776.8, 50942.7, -1636.02))
vars.Gendarran:AddTransition(50, WorldPos(9395.78, -21725.8, -180.709), WorldPos(-6147.82, 16948.2, -1219.81))
vars.Gendarran:AddWaypoint(224, WorldPos(-45415, 13551, -367))
vars.Gendarran:AddWaypoint(225, WorldPos(-5497, -16657, -179))
vars.Gendarran:AddWaypoint(226, WorldPos(-4008.43, -4520.9, 1459.79))
vars.Gendarran:AddWaypoint(227, WorldPos(9890, -18240, -85))
vars.Gendarran:AddWaypoint(228, WorldPos(7092, 19892, -323))
vars.Gendarran:AddWaypoint(395, WorldPos(-37497.6, -12240, -491.137))
vars.Gendarran:AddWaypoint(396, WorldPos(-36517.3, -759.536, -347.122))
vars.Gendarran:AddWaypoint(237, WorldPos(42963, -5809, -2619))
vars.Gendarran:AddWaypoint(238, WorldPos(45285.3, 12139.1, -2699.13))
vars.Gendarran:AddWaypoint(399, WorldPos(-13453.9, 85.1408, -203.705))
vars.Gendarran:AddWaypoint(400, WorldPos(-745.718, 8291.75, -65.9327))
vars.Gendarran:AddWaypoint(401, WorldPos(14871, -6266, -515))
vars.Gendarran:AddWaypoint(402, WorldPos(30021.8, 18007.9, -3107.52))
vars.Gendarran:AddWaypoint(403, WorldPos(41415, -19546, -2903))
vars.Gendarran:AddWaypoint(404, WorldPos(26293, -3113, -720))
vars.Gendarran:AddWaypoint(1026, WorldPos(-21535, 4679.74, -506.853))
vars.Gendarran:AddWaypoint(973, WorldPos(9027.28, 10032.5, -97.2993))
vars.Gendarran:AddWaypoint(923, WorldPos(-22346.7, -10109.9, -264.704))
vars.Gendarran:AddWaypoint(972, WorldPos(34600.4, 9812.65, -1844.11))
vars.Gendarran:AddWaypoint(398, WorldPos(-30026, 3291, -1278))
vars.Gendarran:AddWaypoint(397, WorldPos(-26412, 22425, -1981))
vars.Gendarran:AddWaypoint(223, WorldPos(-43356.3, -17296.5, -345.646))

vars.IronMarches = Map:new(25, "Iron Marches", 50)
vars.IronMarches:AddTransition(19, WorldPos(838.202, -47257.4, -1133.26), WorldPos(26127.3, 21961.8, -1508.02))
vars.IronMarches:AddTransition(22, WorldPos(-22177.1, 18312.2, -1092.33), WorldPos(37751.8, -21940.3, -307.922))
vars.IronMarches:AddTransition(20, WorldPos(22676.3, -22349.4, -474.541), WorldPos(-20772, 38143, -699.17))
vars.IronMarches:AddWaypoint(482, WorldPos(303.248, -46064.2, -1032.89))
vars.IronMarches:AddWaypoint(483, WorldPos(14304, -40647, -194))
vars.IronMarches:AddWaypoint(484, WorldPos(-15518, -35764, -1735))
vars.IronMarches:AddWaypoint(485, WorldPos(4547.45, -21232.9, -248.728))
vars.IronMarches:AddWaypoint(486, WorldPos(21322.6, -21668.1, -421.249))
vars.IronMarches:AddWaypoint(487, WorldPos(-8212.5, -15188.3, -129.854))
vars.IronMarches:AddWaypoint(488, WorldPos(-17516.5, -6076.02, -1331.14))
vars.IronMarches:AddWaypoint(489, WorldPos(7228.76, -4254.1, -186.692))
vars.IronMarches:AddWaypoint(490, WorldPos(5748.11, 21769.1, -885.92))
vars.IronMarches:AddWaypoint(491, WorldPos(-6865.67, 11652.2, -762.587))
vars.IronMarches:AddWaypoint(492, WorldPos(-21166, 16716.7, -740.067))
vars.IronMarches:AddWaypoint(493, WorldPos(-4266.13, 22904.2, -93.8723))
vars.IronMarches:AddWaypoint(494, WorldPos(-6048.68, 38918.8, -841.477))
vars.IronMarches:AddWaypoint(495, WorldPos(17042, 35186, -397))

vars.Dredgehaunt = Map:new(26, "Dredgehaunt Cliffs", 40)
vars.Dredgehaunt:AddTransition(27, WorldPos(-23747.5, 6238.24, -2275.37), WorldPos(18541.5, -20043.1, -4941.04))
vars.Dredgehaunt:AddTransition(29, WorldPos(1385.13, -31977.1, -1831.59), WorldPos(12349.4, 37901.8, -2738.79))
vars.Dredgehaunt:AddTransition(326, WorldPos(8029.95, 36608.4, -4018.43), WorldPos(11631.6, -12115.4, -13081.9))
vars.Dredgehaunt:AddWaypoint(608, WorldPos(22800.8, 23766.2, -3235.27))
vars.Dredgehaunt:AddWaypoint(609, WorldPos(13361, 21504, -2503.55))
vars.Dredgehaunt:AddWaypoint(610, WorldPos(4745, 32988, -3900))
vars.Dredgehaunt:AddWaypoint(611, WorldPos(-2497.12, 27019.2, -2597.58))
vars.Dredgehaunt:AddWaypoint(612, WorldPos(-11199.4, 29152.5, -2297.71))
vars.Dredgehaunt:AddWaypoint(613, WorldPos(-16458.3, 20821.3, -1522.13))
vars.Dredgehaunt:AddWaypoint(1262, WorldPos(20093.6, -16406.9, -3235.67))
vars.Dredgehaunt:AddWaypoint(1343, WorldPos(-20151, -25242.6, 8.66776))
vars.Dredgehaunt:AddWaypoint(598, WorldPos(-11675.1, -26240.8, -961.783))
vars.Dredgehaunt:AddWaypoint(599, WorldPos(1225.54, -30397.9, -2169.39))
vars.Dredgehaunt:AddWaypoint(600, WorldPos(15174.9, -24256.4, -2258.68))
vars.Dredgehaunt:AddWaypoint(601, WorldPos(10569.5, -5744.93, -3721.11))
vars.Dredgehaunt:AddWaypoint(602, WorldPos(1838, -11167, -4633.05))
vars.Dredgehaunt:AddWaypoint(603, WorldPos(-16114.4, -11161, -3481.86))
vars.Dredgehaunt:AddWaypoint(604, WorldPos(-22198, 7896, -2265))
vars.Dredgehaunt:AddWaypoint(605, WorldPos(-7212, 14279, -2943))
vars.Dredgehaunt:AddWaypoint(606, WorldPos(4772, 7265, -3468))
vars.Dredgehaunt:AddWaypoint(607, WorldPos(18609, 9901, -3008))

vars.LornarsPass = Map:new(27, "Lornar's Pass", 25)
vars.LornarsPass:AddTransition(24, WorldPos(-19228.1, 51454, -1620.41), WorldPos(47458.3, -7449.66, -3324.35))
vars.LornarsPass:AddTransition(31, WorldPos(13145.2, 56243.3, -6375.35), WorldPos(-13056.6, -21171.6, -1881.83))
vars.LornarsPass:AddTransition(73, WorldPos(-18617, -6198.58, -2025.67), WorldPos(27355.2, 34962.4, -1510.2))
vars.LornarsPass:AddTransition(26, WorldPos(18850.5, -20092.1, -5024.67), WorldPos(-23256.3, 6456.98, -2323.39))
vars.LornarsPass:AddTransition(29, WorldPos(11383, -55764.8, -2893.95), WorldPos(-21569.5, 37076.5, -1736.41))
vars.LornarsPass:AddTransition(73, WorldPos(-18334.5, -50014.3, 41.5898), WorldPos(28653.4, -6363.25, -134))
vars.LornarsPass:AddTransition(50, WorldPos(-18584, 14958.4, -5173.28), WorldPos(25165.2, 1910.65, -1035.39))
vars.LornarsPass:AddWaypoint(229, WorldPos(12425.3, -46348, -3441.38))
vars.LornarsPass:AddWaypoint(230, WorldPos(-15828.8, -46889.6, -1641.55))
vars.LornarsPass:AddWaypoint(231, WorldPos(16628.1, -21103.4, -4494.95))
vars.LornarsPass:AddWaypoint(232, WorldPos(-13042, -26404.7, -1367.34))
vars.LornarsPass:AddWaypoint(233, WorldPos(-16516.1, 19521.8, -6650.3))
vars.LornarsPass:AddWaypoint(234, WorldPos(17483.5, 12513.6, -6462.02))
vars.LornarsPass:AddWaypoint(235, WorldPos(11452.6, 53716.5, -5973.07))
vars.LornarsPass:AddWaypoint(236, WorldPos(-16846, 49182, -1995.52))
vars.LornarsPass:AddWaypoint(1617, WorldPos(-6512.91, 48725.5, -2568.34))
vars.LornarsPass:AddWaypoint(1618, WorldPos(-6933.35, 15692.4, -3682.66))
vars.LornarsPass:AddWaypoint(1619, WorldPos(-1251.35, 5859.38, -4108.36))
vars.LornarsPass:AddWaypoint(1620, WorldPos(-4638.2, -15568.1, -2501.58))
vars.LornarsPass:AddWaypoint(406, WorldPos(-2920.47, -36081.7, -2419.67))
vars.LornarsPass:AddWaypoint(407, WorldPos(-16788.5, -6013.11, -1981.21))
vars.LornarsPass:AddWaypoint(408, WorldPos(7624.24, 28475.7, -5345.94))
vars.LornarsPass:AddWaypoint(409, WorldPos(-2848, 39543, -3395))
vars.LornarsPass:AddWaypoint(1841, WorldPos(6430.78, -9391.7, -3265.28))

vars.WayfarerFoothills = Map:new(28, "Wayfarer Foothills", 1)
vars.WayfarerFoothills:AddTransition(326, WorldPos(-12217.8, -22148.2, -5017.49), WorldPos(19335.1, -7316.8, -12257.7))
vars.WayfarerFoothills:AddTransition(31, WorldPos(-15015.9, 42512.4, -4722.57), WorldPos(46044, 16630.1, -2309.06))
vars.WayfarerFoothills:AddTransition(32, WorldPos(16821.7, 38384.2, -4946.47), WorldPos(-39541.5, 9908.98, -4820.36))
vars.WayfarerFoothills:AddTransition(30, WorldPos(10923.7, 50828.1, -8410.63), WorldPos(18813.7, -33888.1, -1847.51))
vars.WayfarerFoothills:AddWaypoint(961, WorldPos(5508, 29721, -3298))
vars.WayfarerFoothills:AddWaypoint(962, WorldPos(9113.5, 20495.1, -3318.78))
vars.WayfarerFoothills:AddWaypoint(963, WorldPos(10868.1, -36040.3, -96.6437))
vars.WayfarerFoothills:AddWaypoint(370, WorldPos(-1545.46, -42835.4, -956.574))
vars.WayfarerFoothills:AddWaypoint(371, WorldPos(597.349, -30352.1, -1563.2))
vars.WayfarerFoothills:AddWaypoint(372, WorldPos(-9057.68, -24303.5, -3736.14))
vars.WayfarerFoothills:AddWaypoint(373, WorldPos(12946, -14804, -915))
vars.WayfarerFoothills:AddWaypoint(374, WorldPos(1779, -7989, -1154))
vars.WayfarerFoothills:AddWaypoint(375, WorldPos(-12692, 5278, -3650))
vars.WayfarerFoothills:AddWaypoint(376, WorldPos(-7401.14, 22729.7, -4283.76))
vars.WayfarerFoothills:AddWaypoint(377, WorldPos(-5016, 32244, -4665))
vars.WayfarerFoothills:AddWaypoint(378, WorldPos(-12880, 40699, -4496))
vars.WayfarerFoothills:AddWaypoint(379, WorldPos(14480, 38424, -4827))
vars.WayfarerFoothills:AddWaypoint(380, WorldPos(10428, 48659, -7760))
vars.WayfarerFoothills:AddWaypoint(381, WorldPos(565, 4834, -3125))
vars.WayfarerFoothills:AddWaypoint(382, WorldPos(11907, 8489, -2781))
vars.WayfarerFoothills:AddWaypoint(1025, WorldPos(-11693, -13427, -5946))

vars.Timberline = Map:new(29, "Timberline Falls", 50)
vars.Timberline:AddTransition(27, WorldPos(-21886.7, 37611.4, -1786.19), WorldPos(11270.9, -55156.6, -2857.19))
vars.Timberline:AddTransition(39, WorldPos(-13539.4, -33214.3, -442.556), WorldPos(-2976.22, 29062.8, -902.976))
vars.Timberline:AddTransition(26, WorldPos(12528.2, 38311.7, -2876.19), WorldPos(1829.04, -30903.9, -1970.95))
vars.Timberline:AddWaypoint(580, WorldPos(-14136, -31991, -71))
vars.Timberline:AddWaypoint(581, WorldPos(-3856, -27558, -467.484))
vars.Timberline:AddWaypoint(582, WorldPos(13519.7, -30560.6, 1471.33))
vars.Timberline:AddWaypoint(583, WorldPos(21147.1, -20454.3, -138.692))
vars.Timberline:AddWaypoint(584, WorldPos(21109, -9010.66, -656.199))
vars.Timberline:AddWaypoint(585, WorldPos(2434.75, -17187.9, -66.929))
vars.Timberline:AddWaypoint(586, WorldPos(8286.96, -6235.08, -536.843))
vars.Timberline:AddWaypoint(587, WorldPos(-10069.9, -12709, -458.044))
vars.Timberline:AddWaypoint(588, WorldPos(-16841, 6597.17, -357.483))
vars.Timberline:AddWaypoint(589, WorldPos(13801, 7800, -276))
vars.Timberline:AddWaypoint(590, WorldPos(23542.2, 21588.6, -1011))
vars.Timberline:AddWaypoint(591, WorldPos(5241.67, 18885.9, -2320.83))
vars.Timberline:AddWaypoint(592, WorldPos(-6470.04, 24742.8, -449.15))
vars.Timberline:AddWaypoint(593, WorldPos(-15101.6, 15023.5, -976.479))
vars.Timberline:AddWaypoint(594, WorldPos(-21843.5, 35914.6, -1522.45))
vars.Timberline:AddWaypoint(595, WorldPos(7712, 34501, -1893.68))
vars.Timberline:AddWaypoint(596, WorldPos(-15033, -20375, -846.022))
vars.Timberline:AddWaypoint(597, WorldPos(-15702.1, -7293.33, -425.655))
vars.Timberline:AddWaypoint(1094, WorldPos(327.206, 505.002, -1799.7))

vars.Frostgorge = Map:new(30, "Frostgorge Sound", 70)
vars.Frostgorge:AddTransition(22, WorldPos(34432.8, 524.948, -504.22), WorldPos(-34505, 2735.85, -3399.48))
vars.Frostgorge:AddTransition(28, WorldPos(18907, -34486.9, -2078.96), WorldPos(10774, 50204, -8283.77))
vars.Frostgorge:AddTransition(31, WorldPos(-29459.4, -35485.6, -1910.27), WorldPos(30413.3, 21142.1, -1568.83))
vars.Frostgorge:AddWaypoint(640, WorldPos(-17765, -3396.69, 4060.64))
vars.Frostgorge:AddWaypoint(641, WorldPos(-8688.67, -21048.9, -1829.82))
vars.Frostgorge:AddWaypoint(642, WorldPos(8551.69, -23127.7, -4017.29))
vars.Frostgorge:AddWaypoint(643, WorldPos(5114.36, -10517.2, -2899.9))
vars.Frostgorge:AddWaypoint(644, WorldPos(23131.5, -12272.9, -935.185))
vars.Frostgorge:AddWaypoint(645, WorldPos(-23070, -13320.5, -245.046))
vars.Frostgorge:AddWaypoint(646, WorldPos(-35227.5, 34807.3, -619.263))
vars.Frostgorge:AddWaypoint(1347, WorldPos(4499.33, 22193.9, -215.825))
vars.Frostgorge:AddWaypoint(632, WorldPos(19020.1, -33042, -1784.9))
vars.Frostgorge:AddWaypoint(633, WorldPos(-27833.2, -34885.3, -1782))
vars.Frostgorge:AddWaypoint(634, WorldPos(30183.1, 5464.01, -1139.13))
vars.Frostgorge:AddWaypoint(635, WorldPos(29208.8, 30370.9, -5015.63))
vars.Frostgorge:AddWaypoint(636, WorldPos(4619, 13980, -1473))
vars.Frostgorge:AddWaypoint(637, WorldPos(12605.7, 27003.1, -183.631))
vars.Frostgorge:AddWaypoint(638, WorldPos(-11886.1, 19505.9, -78))
vars.Frostgorge:AddWaypoint(639, WorldPos(-24938, 14751.9, -38.6558))

vars.SnowdenDrifts = Map:new(31, "Snowden Drifts", 15)
vars.SnowdenDrifts:AddTransition(24, WorldPos(-46361.4, -11193.5, -1405.64), WorldPos(48794, 13282.1, -3125.18))
vars.SnowdenDrifts:AddTransition(28, WorldPos(46409.7, 17038.1, -2423.38), WorldPos(-14246.3, 42156.4, -4564.73))
vars.SnowdenDrifts:AddTransition(30, WorldPos(30367.1, 21642.2, -1659.32), WorldPos(-29395.7, -35256.1, -1908.6))
vars.SnowdenDrifts:AddTransition(27, WorldPos(-12796.2, -21644.4, -2083.32), WorldPos(12617.7, 55914.4, -6199.99))
vars.SnowdenDrifts:AddWaypoint(192, WorldPos(35905.6, -10227.1, -3552.21))
vars.SnowdenDrifts:AddWaypoint(193, WorldPos(42399, -15267, -4370))
vars.SnowdenDrifts:AddWaypoint(194, WorldPos(-38401.9, -15279.3, -2307.62))
vars.SnowdenDrifts:AddWaypoint(1791, WorldPos(-45316.7, 9354.81, -5232.12))
vars.SnowdenDrifts:AddWaypoint(960, WorldPos(-2535.98, -13746.2, -165.177))
vars.SnowdenDrifts:AddWaypoint(959, WorldPos(-29364.2, -1531.9, -1664.62))
vars.SnowdenDrifts:AddWaypoint(179, WorldPos(-45252.4, -9952.72, -1612.8))
vars.SnowdenDrifts:AddWaypoint(180, WorldPos(-15972.5, -16648.5, -1162.76))
vars.SnowdenDrifts:AddWaypoint(181, WorldPos(44759.2, 15021.7, -2299.75))
vars.SnowdenDrifts:AddWaypoint(182, WorldPos(25614.3, 20494.3, -2426.68))
vars.SnowdenDrifts:AddWaypoint(183, WorldPos(-43395, 12654, -5081))
vars.SnowdenDrifts:AddWaypoint(184, WorldPos(-17510.4, 15213.5, -129.627))
vars.SnowdenDrifts:AddWaypoint(185, WorldPos(-12738.8, -2595.73, -1833.42))
vars.SnowdenDrifts:AddWaypoint(186, WorldPos(-43072, 4127, -3440))
vars.SnowdenDrifts:AddWaypoint(187, WorldPos(8810.46, 5597.25, -3145.25))
vars.SnowdenDrifts:AddWaypoint(188, WorldPos(13281, -16764, -1217))
vars.SnowdenDrifts:AddWaypoint(189, WorldPos(13514.8, 20418.8, -2404.97))
vars.SnowdenDrifts:AddWaypoint(190, WorldPos(12588, 12597, -2535))
vars.SnowdenDrifts:AddWaypoint(191, WorldPos(28666, 2481, -2198))

vars.Diessa = Map:new(32, "Diessa Plateau", 15)
vars.Diessa:AddTransition(28, WorldPos(-39941.1, 10094.8, -5048.64), WorldPos(16190.8, 38730.9, -4969.18))
vars.Diessa:AddTransition(218, WorldPos(-30107.9, -25191.9, -1796.06), WorldPos(-477.567, 20711.5, -5427.61))
vars.Diessa:AddTransition(19, WorldPos(36081.7, -26209.5, -552.592), WorldPos(-9837.98, 21296.4, -2469.07))
vars.Diessa:AddWaypoint(352, WorldPos(-14542, -7138, -1676))
vars.Diessa:AddWaypoint(353, WorldPos(8932.14, -21015.7, -930.977))
vars.Diessa:AddWaypoint(354, WorldPos(29895, 17321, -1369))
vars.Diessa:AddWaypoint(355, WorldPos(37006, -6806, -1532))
vars.Diessa:AddWaypoint(964, WorldPos(-16287, 17133, -2671))
vars.Diessa:AddWaypoint(965, WorldPos(33080, 5640, -1634))
vars.Diessa:AddWaypoint(966, WorldPos(19804, -9438, -1363))
vars.Diessa:AddWaypoint(969, WorldPos(1956, 13007, -1064))
vars.Diessa:AddWaypoint(1090, WorldPos(15173, 12678, -43))
vars.Diessa:AddWaypoint(356, WorldPos(32601, -24682, -312))
vars.Diessa:AddWaypoint(922, WorldPos(8839, -4317, -1477))
vars.Diessa:AddWaypoint(350, WorldPos(-28902, -9394, -1243))
vars.Diessa:AddWaypoint(217, WorldPos(-37382.6, 8524.29, -3922.12))
vars.Diessa:AddWaypoint(218, WorldPos(-6743.01, 231.495, -1386.97))
vars.Diessa:AddWaypoint(219, WorldPos(20235.6, 5659.31, -941.867))
vars.Diessa:AddWaypoint(220, WorldPos(15623, 22563, -1484))
vars.Diessa:AddWaypoint(221, WorldPos(-8748, -18549, -1639))
vars.Diessa:AddWaypoint(222, WorldPos(-28244, -21261, -1234))
vars.Diessa:AddWaypoint(351, WorldPos(-24446, 10869, -2439))

vars.Caledon = Map:new(34, "Caledon Forest", 1)
vars.Caledon:AddTransition(23, WorldPos(19723.9, 47623.2, -712.011), WorldPos(-6917.69, -21856.7, -1795.94))
vars.Caledon:AddTransition(54, WorldPos(-19158.4, 44582.2, -420.887), WorldPos(40301, -11882.1, -1467.52))
vars.Caledon:AddTransition(91, WorldPos(-7539.06, -44696.2, -929.896), WorldPos(-3105.11, 10736.5, -2901.06))
vars.Caledon:AddWaypoint(320, WorldPos(11515, 40840, -335))
vars.Caledon:AddWaypoint(321, WorldPos(-18129.1, 45124.3, -278.182))
vars.Caledon:AddWaypoint(322, WorldPos(263, -25993, -715))
vars.Caledon:AddWaypoint(844, WorldPos(-2970, 33732, -646))
vars.Caledon:AddWaypoint(1345, WorldPos(-15759.9, 38290.2, -16.1856))
vars.Caledon:AddWaypoint(318, WorldPos(-13485, 27625, -156))
vars.Caledon:AddWaypoint(308, WorldPos(-6818.02, -39771.8, -172.532))
vars.Caledon:AddWaypoint(309, WorldPos(17321.7, -25446.5, -483.526))
vars.Caledon:AddWaypoint(310, WorldPos(-9309.34, -28190.7, -1731.69))
vars.Caledon:AddWaypoint(311, WorldPos(-11360, -21594, -171))
vars.Caledon:AddWaypoint(312, WorldPos(1278, -12179, -1227))
vars.Caledon:AddWaypoint(313, WorldPos(18035, -9967, -889))
vars.Caledon:AddWaypoint(314, WorldPos(-3332, 648, -121))
vars.Caledon:AddWaypoint(315, WorldPos(-16933, 13595, -1596))
vars.Caledon:AddWaypoint(316, WorldPos(-8365, 17609, -2212))
vars.Caledon:AddWaypoint(317, WorldPos(14062, 9530, -695))
vars.Caledon:AddWaypoint(1534, WorldPos(12188.8, -34105.9, -1095.53))
vars.Caledon:AddWaypoint(319, WorldPos(8099.83, 32689.5, -405.721))

vars.Metrica = Map:new(35, "Metrica Province", 1)
vars.Metrica:AddTransition(54, WorldPos(-2624.94, 37118.6, -1252.73), WorldPos(10986.7, -27307.1, -2956.12))
vars.Metrica:AddTransition(139, WorldPos(-21861.2, -29596.3, -2088.71), WorldPos(5259.42, 3585.23, -15060))
vars.Metrica:AddWaypoint(64, WorldPos(-18920.2, -28859.7, -1859.64))
vars.Metrica:AddWaypoint(65, WorldPos(5805.96, -34469.2, -691.201))
vars.Metrica:AddWaypoint(66, WorldPos(18698, -19520, -647))
vars.Metrica:AddWaypoint(67, WorldPos(-4839, -5106, -1103.33))
vars.Metrica:AddWaypoint(68, WorldPos(7717, 10315, -1297.47))
vars.Metrica:AddWaypoint(69, WorldPos(20689.7, 26699.2, -1383.16))
vars.Metrica:AddWaypoint(70, WorldPos(-9169.65, 17621.4, -485.754))
vars.Metrica:AddWaypoint(71, WorldPos(-2863.35, 35470.1, -1149.42))
vars.Metrica:AddWaypoint(72, WorldPos(8342.37, -5693.24, -117.828))
vars.Metrica:AddWaypoint(1271, WorldPos(-7877.54, -24401.9, -738.922))
vars.Metrica:AddWaypoint(1203, WorldPos(1338.39, 20696.1, -245.696))
vars.Metrica:AddWaypoint(1200, WorldPos(-20767.4, 13983.4, -1727.81))
vars.Metrica:AddWaypoint(1201, WorldPos(-6375.41, 4790.15, -454.509))
vars.Metrica:AddWaypoint(1202, WorldPos(22731.5, 1783.92, -1086.4))
vars.Metrica:AddWaypoint(1198, WorldPos(-13723, -13180.6, -344.996))
vars.Metrica:AddWaypoint(1199, WorldPos(1826.16, -20742, -806.593))

vars.Maelstrom = Map:new(39, "Mount Maelstrom", 60)
vars.Maelstrom:AddTransition(53, WorldPos(-43883.3, 21889.7, -1485.44), WorldPos(26515.8, -25585.3, -1606.57))
vars.Maelstrom:AddTransition(51, WorldPos(-44239.7, -19308.3, -648.426), WorldPos(36416.7, 6353.88, -1076.23))
vars.Maelstrom:AddTransition(29, WorldPos(-3085.43, 29431.4, -1062.41), WorldPos(-13557.7, -32784.3, -359.442))
vars.Maelstrom:AddWaypoint(1346, WorldPos(30317.1, 17215.4, -463.037))
vars.Maelstrom:AddWaypoint(711, WorldPos(-41749.6, -15165.4, -1640.86))
vars.Maelstrom:AddWaypoint(712, WorldPos(-42570.1, 22171.8, -1474.46))
vars.Maelstrom:AddWaypoint(713, WorldPos(-35338.7, 14363.2, -2888))
vars.Maelstrom:AddWaypoint(714, WorldPos(-27682.8, 1930.8, -1188.31))
vars.Maelstrom:AddWaypoint(715, WorldPos(-28415, -7690.66, -2330.82))
vars.Maelstrom:AddWaypoint(716, WorldPos(-16385, 22379, -1944))
vars.Maelstrom:AddWaypoint(717, WorldPos(-11825, -11402, -432))
vars.Maelstrom:AddWaypoint(718, WorldPos(-7619.05, 11999.7, -2495.39))
vars.Maelstrom:AddWaypoint(719, WorldPos(-640, -12597, -3061))
vars.Maelstrom:AddWaypoint(720, WorldPos(-2588.53, 28339, -562.942))
vars.Maelstrom:AddWaypoint(721, WorldPos(7538.67, 1145, -112.069))
vars.Maelstrom:AddWaypoint(722, WorldPos(17160.2, -13988.9, -674.997))
vars.Maelstrom:AddWaypoint(723, WorldPos(11703.8, 26243.2, -2212.68))
vars.Maelstrom:AddWaypoint(724, WorldPos(34500, -6250, -5.11935))
vars.Maelstrom:AddWaypoint(725, WorldPos(41779, -24254, -56))
vars.Maelstrom:AddWaypoint(726, WorldPos(-16966, -20411, -3574))

vars.LionsArch = Map:new(50, "Lion's Arch")
vars.LionsArch:AddTransition(24, WorldPos(-6330.42, 17373.1, -1297.89), WorldPos(9294.67, -21172.8, -104.197))
vars.LionsArch:AddTransition(91, WorldPos(5711.6, 6929.28, -1548.06), WorldPos(2098.67, 4528.17, -2823.85))
vars.LionsArch:AddTransition(18, WorldPos(5010.56, 10514.6, -1548.38), WorldPos(4723.31, -5929.87, -5651.51))
vars.LionsArch:AddTransition(73, WorldPos(10563.1, -15832.6, -1273.74), WorldPos(8991.27, 38468.9, -762.864))
vars.LionsArch:AddTransition(27, WorldPos(25575.3, 1735.31, -1037.16), WorldPos(-18371, 14816.4, -5155.93))
vars.LionsArch:AddTransition(873, WorldPos(6303.41, 10598.5, -1546.6), WorldPos(25666.7, -11879.9, -1329.95))
vars.LionsArch:AddTransition(326, WorldPos(7088, 9714.28, -1547.14), WorldPos(-2199.86, -1519.92, -12360.7))
vars.LionsArch:AddTransition(218, WorldPos(6717.22, 7301.22, -1552.26), WorldPos(-6204.93, 12540.4, -6831.45))
vars.LionsArch:AddTransition(139, WorldPos(4007.32, 8251.78, -1546.61), WorldPos(2614.74, 10962.4, -15062.2))
vars.LionsArch:AddWaypoint(1040, WorldPos(-2122.12, 13847.6, -822.351))
vars.LionsArch:AddWaypoint(1073, WorldPos(14344.8, 12644, -1213.16))
vars.LionsArch:AddWaypoint(1074, WorldPos(24248.3, 1241.47, -1283.72))
vars.LionsArch:AddWaypoint(1075, WorldPos(19322.3, -4859.1, -993))
vars.LionsArch:AddWaypoint(1071, WorldPos(-11724.3, 2230.27, -348.733))
vars.LionsArch:AddWaypoint(1070, WorldPos(-17987.1, 7954.87, -574.937))
vars.LionsArch:AddWaypoint(1069, WorldPos(-22129.3, -4810.81, -1644.4))
vars.LionsArch:AddWaypoint(1072, WorldPos(7327.32, -5088.03, -1201.2))
vars.LionsArch:AddWaypoint(1041, WorldPos(3770.73, 6364.67, -1194.48))
vars.LionsArch:AddWaypoint(1036, WorldPos(-13944.8, 14871.3, -1215.69))
vars.LionsArch:AddWaypoint(1037, WorldPos(18603.6, 8389.51, -809.123))
vars.LionsArch:AddWaypoint(1038, WorldPos(11228.1, -13346.9, -1215.27))
vars.LionsArch:AddWaypoint(1039, WorldPos(-3723.92, -8764.75, -400.971))

vars.Straits = Map:new(51, "Straits of Devastation", 70)
vars.Straits:AddTransition(65, WorldPos(-37268.2, -17610.6, -2124.15), WorldPos(46167, -10104.5, -2138.96))
vars.Straits:AddTransition(53, WorldPos(20444.2, 30250.5, -3049.04), WorldPos(11196.5, -35176.3, -2041.82))
vars.Straits:AddTransition(39, WorldPos(37276.3, 6458.89, -1093.19), WorldPos(-43799.7, -19036.5, -656.31))
vars.Straits:AddWaypoint(1765, WorldPos(-28545.1, 16565.3, 837.074))
vars.Straits:AddWaypoint(748, WorldPos(20039.3, 28635.7, -2833.67))
vars.Straits:AddWaypoint(749, WorldPos(5424.45, 27369.7, -207.957))
vars.Straits:AddWaypoint(750, WorldPos(27966.2, 4510.16, -754.434))
vars.Straits:AddWaypoint(751, WorldPos(35744, 5803, -1167))
vars.Straits:AddWaypoint(752, WorldPos(24792.5, -5090.83, -209.424))
vars.Straits:AddWaypoint(753, WorldPos(11619.8, -18492, -45.6607))
vars.Straits:AddWaypoint(754, WorldPos(4235.49, 2962.17, 2607.85))
vars.Straits:AddWaypoint(755, WorldPos(-8592.46, 15999.5, 1724.12))
vars.Straits:AddWaypoint(756, WorldPos(-13134.5, 8157.73, -3.37552))
vars.Straits:AddWaypoint(757, WorldPos(-31050.4, -1437.58, -1370.35))
vars.Straits:AddWaypoint(758, WorldPos(-8743.84, -3588.83, -13.0934))
vars.Straits:AddWaypoint(759, WorldPos(957, -12124, -61))
vars.Straits:AddWaypoint(760, WorldPos(-8724.75, -26971.3, -979.396))
vars.Straits:AddWaypoint(761, WorldPos(-15508.1, -15691.5, -331.152))
vars.Straits:AddWaypoint(762, WorldPos(-25844.6, -18424, -3844.53))
vars.Straits:AddWaypoint(763, WorldPos(-35411.3, -14870.6, -1472.27))
vars.Straits:AddWaypoint(1625, WorldPos(20178.9, 22168.2, -55.9686))
vars.Straits:AddWaypoint(1624, WorldPos(28060.9, 21770.8, -401.273))
vars.Straits:AddWaypoint(1234, WorldPos(-19766.6, -4687.05, -1593.85))

vars.Sparkfly = Map:new(53, "Sparkfly Fen", 55)
vars.Sparkfly:AddTransition(51, WorldPos(11195.9, -35612.2, -2196.03), WorldPos(20330.9, 29655.3, -2913.35))
vars.Sparkfly:AddTransition(73, WorldPos(20053.9, 36467.84, -117.6), WorldPos(21019.7, -32602.3, -175.823))
vars.Sparkfly:AddTransition(39, WorldPos(27203.3, -25291.8, -1719.66), WorldPos(-43362.6, 21799.5, -1485.79))
vars.Sparkfly:AddWaypoint(452, WorldPos(-20641, 34955, -180))
vars.Sparkfly:AddWaypoint(453, WorldPos(7858, 33556, -1819.73))
vars.Sparkfly:AddWaypoint(454, WorldPos(20369, 32006, -211.237))
vars.Sparkfly:AddWaypoint(455, WorldPos(19102, 21251, -1462))
vars.Sparkfly:AddWaypoint(456, WorldPos(-971, 16024, -1193))
vars.Sparkfly:AddWaypoint(457, WorldPos(-13394.5, 20541.6, -817.505))
vars.Sparkfly:AddWaypoint(458, WorldPos(-23583, 2581, -269))
vars.Sparkfly:AddWaypoint(459, WorldPos(2839.01, 1483.21, -336.181))
vars.Sparkfly:AddWaypoint(460, WorldPos(22899, 865, -1633.22))
vars.Sparkfly:AddWaypoint(461, WorldPos(22783, -23980, -1301))
vars.Sparkfly:AddWaypoint(462, WorldPos(12235.5, -28810.5, -11.9044))
vars.Sparkfly:AddWaypoint(463, WorldPos(-2767, -13634, -349))
vars.Sparkfly:AddWaypoint(464, WorldPos(-15178.3, -29773.8, -305.534))
vars.Sparkfly:AddWaypoint(465, WorldPos(-21584.2, -15215.7, 1817.51))
vars.Sparkfly:AddWaypoint(947, WorldPos(-478, -26214, -46))
vars.Sparkfly:AddWaypoint(948, WorldPos(8322, 27701, -65.2865))
vars.Sparkfly:AddWaypoint(845, WorldPos(13015, -12658, -79))

vars.Brisban = Map:new(54, "Brisban Wildlands", 15)
vars.Brisban:AddTransition(23, WorldPos(40010.6, 28938.7, -2590.6), WorldPos(-45135.8, 11786.2, -2971.82))
vars.Brisban:AddTransition(34, WorldPos(40701.1, -11902.6, -1468.53), WorldPos(-18647.8, 44600.9, -314.497))
vars.Brisban:AddTransition(35, WorldPos(11007.2, -27605.3, -3163.34), WorldPos(-2890.22, 36628.8, -1201.02))
vars.Brisban:AddTransition(1015, WorldPos(-37862.4, 6429.04, -1752.09), WorldPos(23533, -7492, -16328.9))
vars.Brisban:AddTransition(988, WorldPos(-36291.1, -29631, -1871.82), WorldPos(16050.2, -20537.7, -1008.59))
vars.Brisban:AddWaypoint(96, WorldPos(22925.5, 6529.61, -2162.02))
vars.Brisban:AddWaypoint(97, WorldPos(-3706.48, 9582.39, -2114.3))
vars.Brisban:AddWaypoint(98, WorldPos(-3106.05, 28705.6, -180.881))
vars.Brisban:AddWaypoint(99, WorldPos(-27477.4, 21836.7, -2612.29))
vars.Brisban:AddWaypoint(100, WorldPos(-31860, -6071.67, -887.156))
vars.Brisban:AddWaypoint(101, WorldPos(-7298.58, -6883.62, -530.227))
vars.Brisban:AddWaypoint(118, WorldPos(-10957.4, -19089.8, -1125.39))
vars.Brisban:AddWaypoint(1785, WorldPos(-33199.3, 17917.7, -2685.14))
vars.Brisban:AddWaypoint(117, WorldPos(9925.9, -25026.4, -2080.05))
vars.Brisban:AddWaypoint(92, WorldPos(36327.3, -10092.2, -1254.38))
vars.Brisban:AddWaypoint(93, WorldPos(38098.2, 25541.8, -2210.66))
vars.Brisban:AddWaypoint(94, WorldPos(12956.8, -4883.78, -1350.53))
vars.Brisban:AddWaypoint(95, WorldPos(23783.2, -1222.12, -2317.31))

vars.CursedShore = Map:new(62, "Cursed Shore", 80)
vars.CursedShore:AddTransition(65, WorldPos(14292.8, 46004.6, -3804.9), WorldPos(-11776.1, -22276.4, -2379.4))
vars.CursedShore:AddWaypoint(800, WorldPos(20420.7, -11215.6, -3496.65))
vars.CursedShore:AddWaypoint(801, WorldPos(4397.22, 36224.9, -1293.32))
vars.CursedShore:AddWaypoint(802, WorldPos(-14743.9, -33814.2, -1708.93))
vars.CursedShore:AddWaypoint(1764, WorldPos(17091, -43034.1, -1054.44))
vars.CursedShore:AddWaypoint(791, WorldPos(14874.3, 44694.4, -3455.28))
vars.CursedShore:AddWaypoint(792, WorldPos(-6534.13, 30149.6, -162.955))
vars.CursedShore:AddWaypoint(793, WorldPos(7307.66, 25191.1, -1665.75))
vars.CursedShore:AddWaypoint(794, WorldPos(-15319.5, 18539.4, -170.634))
vars.CursedShore:AddWaypoint(795, WorldPos(1190.26, 11397.6, -69.0296))
vars.CursedShore:AddWaypoint(796, WorldPos(13733.1, 16185.4, -1770.59))
vars.CursedShore:AddWaypoint(797, WorldPos(10448.4, 5864.11, -1213.92))
vars.CursedShore:AddWaypoint(798, WorldPos(-1204.44, -2492, -1143.06))
vars.CursedShore:AddWaypoint(799, WorldPos(-3910.61, -15087.5, -751.905))

vars.MalchorsLeap = Map:new(65, "Malchor's Leap", 75)
vars.MalchorsLeap:AddTransition(62, WorldPos(-11762.5, -22489.5, -2416.99), WorldPos(14799.7, 45385.6, -3670.78))
vars.MalchorsLeap:AddTransition(51, WorldPos(46494.1, -9816.55, -2113.44), WorldPos(-36708.2, -17294.1, -2025.99))
vars.MalchorsLeap:AddWaypoint(688, WorldPos(-7087.93, -3038.84, -4142.44))
vars.MalchorsLeap:AddWaypoint(689, WorldPos(-30066.9, 14949.1, 1661.74))
vars.MalchorsLeap:AddWaypoint(690, WorldPos(-16671.7, -5982.21, -229.058))
vars.MalchorsLeap:AddWaypoint(678, WorldPos(40377.7, -14434.4, -1200.14))
vars.MalchorsLeap:AddWaypoint(679, WorldPos(-13021.9, -16613.4, -1986.98))
vars.MalchorsLeap:AddWaypoint(680, WorldPos(40576.8, 2756.14, -300.635))
vars.MalchorsLeap:AddWaypoint(681, WorldPos(28885.7, -5898.75, -1759.38))
vars.MalchorsLeap:AddWaypoint(682, WorldPos(23871.6, -15878, -1865.79))
vars.MalchorsLeap:AddWaypoint(683, WorldPos(19908.3, 15100.2, -3940.91))
vars.MalchorsLeap:AddWaypoint(684, WorldPos(7462.33, -3995.15, -1631.52))
vars.MalchorsLeap:AddWaypoint(685, WorldPos(18852.7, 6067.79, -3745.03))
vars.MalchorsLeap:AddWaypoint(686, WorldPos(-1243.96, 1270.33, -1439.11))
vars.MalchorsLeap:AddWaypoint(687, WorldPos(-13021.2, 10861.2, -82.8601))

vars.Bloodtide = Map:new(73, "Bloodtide Coast", 45)
vars.Bloodtide:AddTransition(27, WorldPos(27899.4, 34895.5, -1522.28), WorldPos(-18091.8, -6424.08, -1978.19))
vars.Bloodtide:AddTransition(27, WorldPos(29262.6, -6273.65, 41.2883), WorldPos(-17890.8, -49687.3, -82))
vars.Bloodtide:AddTransition(53, WorldPos(20926.6, -33119.2, -343.302), WorldPos(20431.9, 35541.4, 0))
vars.Bloodtide:AddTransition(50, WorldPos(9206.15, 38760.4, -853.121), WorldPos(10286.7, -15149.2, -1233.59))
vars.Bloodtide:AddWaypoint(432, WorldPos(21146.8, -31784.9, -56.4306))
vars.Bloodtide:AddWaypoint(1035, WorldPos(9557.47, 27062.5, -30.8818))
vars.Bloodtide:AddWaypoint(419, WorldPos(8734, 37978.1, -588.964))
vars.Bloodtide:AddWaypoint(420, WorldPos(-9571, 27360, 1952))
vars.Bloodtide:AddWaypoint(421, WorldPos(2232.33, 20346.5, -1448.89))
vars.Bloodtide:AddWaypoint(422, WorldPos(23053.6, 33304, -1871.52))
vars.Bloodtide:AddWaypoint(423, WorldPos(20004, 12365, 1))
vars.Bloodtide:AddWaypoint(424, WorldPos(-5666, 3548, -54.5416))
vars.Bloodtide:AddWaypoint(425, WorldPos(-19058, -5640, -60))
vars.Bloodtide:AddWaypoint(426, WorldPos(5985, -3637, -343))
vars.Bloodtide:AddWaypoint(427, WorldPos(25084, -3962, -32))
vars.Bloodtide:AddWaypoint(428, WorldPos(24775.2, -18626.1, -446.754))
vars.Bloodtide:AddWaypoint(429, WorldPos(-3808, -19586, -238))
vars.Bloodtide:AddWaypoint(430, WorldPos(-3668.18, -24839.3, 5440.02))
vars.Bloodtide:AddWaypoint(431, WorldPos(-20089, -30057, -693))

vars.Grove = Map:new(91, "The Grove")
vars.Grove:AddTransition(34, WorldPos(-3342.53, 11616.7, -2986.52), WorldPos(-7435.66, -44098.6, -747.345))
vars.Grove:AddTransition(50, WorldPos(1977.81, 4977.53, -2934.7), WorldPos(5796.79, 7314.14, -1544.56))
vars.Grove:AddWaypoint(1212, WorldPos(1859.86, 1325.92, -51.9694))
vars.Grove:AddWaypoint(1210, WorldPos(618.939, 230.143, -2716.46))
vars.Grove:AddWaypoint(1042, WorldPos(-2049.32, 10564, -2796.55))
vars.Grove:AddWaypoint(1211, WorldPos(858.358, 1404.14, -1846.76))

vars.RataSum = Map:new(139, "Rata Sum")
vars.RataSum:AddTransition(35, WorldPos(5439.47, 3665.8, -15090.5), WorldPos(-21404, -29343.5, -2086.97))
vars.RataSum:AddTransition(50, WorldPos(2776.95, 11021.3, -15096.3), WorldPos(4330.98, 8326.94, -1545))
vars.RataSum:AddWaypoint(1043, WorldPos(4702.25, 4712.95, -15189.6))
vars.RataSum:AddWaypoint(1204, WorldPos(-1788.22, 5855.06, -15189.1))
vars.RataSum:AddWaypoint(1205, WorldPos(1115.22, -14097.9, -4243.21))
vars.RataSum:AddWaypoint(1206, WorldPos(2083.41, 6622.8, -14325.5))
vars.RataSum:AddWaypoint(1207, WorldPos(4764.16, 6327.47, -13359.9))
vars.RataSum:AddWaypoint(1208, WorldPos(-1375.31, 7425.65, -13360.7))
vars.RataSum:AddWaypoint(1209, WorldPos(2153.2, 3318.05, -13362.1))
vars.RataSum:AddWaypoint(1044, WorldPos(2858.72, 9705.02, -15189.6))
vars.RataSum:AddWaypoint(1287, WorldPos(-13294.1, -21046.9, -1404.82))

vars.BlackCitadel = Map:new(218, "Black Citadel")
vars.BlackCitadel:AddTransition(32, WorldPos(-442.38, 21786.6, -5500.49), WorldPos(-29903.3, -24689.1, -1781.47))
vars.BlackCitadel:AddTransition(19, WorldPos(17514.5, 9417.1, -5128.21), WorldPos(-44471.9, 5213.83, -2608.11))
vars.BlackCitadel:AddTransition(50, WorldPos(-6486.93, 12772.4, -6909.31), WorldPos(6602.13, 7626.77, -1544.88))
vars.BlackCitadel:AddWaypoint(932, WorldPos(775, -3126, -3950))
vars.BlackCitadel:AddWaypoint(933, WorldPos(15490.1, 8463.77, -5230.02))
vars.BlackCitadel:AddWaypoint(934, WorldPos(1200.22, 14583.8, -6668.84))
vars.BlackCitadel:AddWaypoint(935, WorldPos(6986, 17306, -5538))
vars.BlackCitadel:AddWaypoint(936, WorldPos(-1391, 19899, -5510.78))
vars.BlackCitadel:AddWaypoint(937, WorldPos(-6165.38, 12037, -6737.67))
vars.BlackCitadel:AddWaypoint(938, WorldPos(-1172.32, 10556.9, -1018))
vars.BlackCitadel:AddWaypoint(939, WorldPos(6703.1, 10154.4, -6610.67))
vars.BlackCitadel:AddWaypoint(940, WorldPos(-4997, -5581, -456))
vars.BlackCitadel:AddWaypoint(941, WorldPos(23.4977, 8003.28, -8423.8))
vars.BlackCitadel:AddWaypoint(1833, WorldPos(-11588.6, 11105.9, -852.577))
vars.BlackCitadel:AddWaypoint(1079, WorldPos(3217.15, 1868.82, -6636.74))

vars.Hoelbrak = Map:new(326, "Hoelbrak")
vars.Hoelbrak:AddTransition(28, WorldPos(19702.1, -7480.53, -12341.8), WorldPos(-11938.2, -22411.6, -4865.9))
vars.Hoelbrak:AddTransition(26, WorldPos(11486.8, -12350.1, -13126.9), WorldPos(7480.58, 36263.1, -3930.54))
vars.Hoelbrak:AddTransition(50, WorldPos(-2551.69, -1530, -12449.6), WorldPos(6810.98, 9715.69, -1545.02))
vars.Hoelbrak:AddWaypoint(912, WorldPos(-7855, 6047, -11396))
vars.Hoelbrak:AddWaypoint(1288, WorldPos(6708.86, -5110.71, -13193.3))
vars.Hoelbrak:AddWaypoint(981, WorldPos(2811, -9520, -11918))
vars.Hoelbrak:AddWaypoint(901, WorldPos(9604.58, 2200.17, -9185.92))
vars.Hoelbrak:AddWaypoint(902, WorldPos(7219.51, -1751.25, -11888.2))
vars.Hoelbrak:AddWaypoint(903, WorldPos(1912.99, 4706.03, -9828.55))
vars.Hoelbrak:AddWaypoint(904, WorldPos(10031, 10060.6, -10454.3))
vars.Hoelbrak:AddWaypoint(905, WorldPos(17129.4, 7322.13, -8589.83))
vars.Hoelbrak:AddWaypoint(906, WorldPos(15828.3, -2430.57, -9888.49))
vars.Hoelbrak:AddWaypoint(907, WorldPos(21473, 13970, -8679))
vars.Hoelbrak:AddWaypoint(908, WorldPos(21279, 1194, -10074))
vars.Hoelbrak:AddWaypoint(909, WorldPos(19152, -7924.91, -12262.8))
vars.Hoelbrak:AddWaypoint(910, WorldPos(11130, -10231, -12964))
vars.Hoelbrak:AddWaypoint(911, WorldPos(-1487.44, -1646.48, -12228.4))

vars.SouthsunCove = Map:new(873, "Southsun Cove", 80)
vars.SouthsunCove:AddTransition(50, WorldPos(25743.9, -11789.6, -1341.45), WorldPos(6093.93, 10350.2, -1545))
vars.SouthsunCove:AddWaypoint(1744, WorldPos(-527.809, 13839.6, -23.8834))
vars.SouthsunCove:AddWaypoint(1746, WorldPos(25673.3, 14692.2, -28.6641))
vars.SouthsunCove:AddWaypoint(1756, WorldPos(19092.1, 3708.48, -1150.33))
vars.SouthsunCove:AddWaypoint(1749, WorldPos(24508.4, -13627.7, -1242.89))
vars.SouthsunCove:AddWaypoint(1752, WorldPos(-21229, -16186.4, -1245.26))
vars.SouthsunCove:AddWaypoint(1751, WorldPos(909.058, -13019.3, -909.777))

vars.DryTop = Map:new(988, "Dry Top", 80)
vars.DryTop:AddTransition(54, WorldPos(16704, -20937.1, -1137.57), WorldPos(-36044.9, -29189.5, -1853.04))
vars.DryTop:AddWaypoint(1920, WorldPos(16011.5, -19991.2, -935.131))
vars.DryTop:AddWaypoint(1914, WorldPos(8554.11, -11084.2, -524.934))
vars.DryTop:AddWaypoint(1928, WorldPos(2369.49, -11307.7, -1305.98))
vars.DryTop:AddWaypoint(1926, WorldPos(-17687.8, -10369.9, -3061.88))
vars.DryTop:AddWaypoint(1943, WorldPos(-22054.2, -11977.6, -3038.26))

vars.Silverwastes = Map:new(1015, "The Silverwastes", 80)
vars.Silverwastes:AddTransition(54, WorldPos(24104.2, -7423.63, -16330.7), WorldPos(-37259.4, 6406.3, -1698.95))
vars.Silverwastes:AddTransition(1052, WorldPos(-16918.1, -12461.9, -16751.3), WorldPos(37123.1, 2386.12, -16417))
vars.Silverwastes:AddWaypoint(1964, WorldPos(883.184, -17054.1, -16558.5))
vars.Silverwastes:AddWaypoint(1978, WorldPos(14437.9, 6058.28, -11137.9))
vars.Silverwastes:AddWaypoint(1919, WorldPos(19658.1, -9036.09, -16228.4))

vars.DragonsStand = Map:new(1041, "Dragon's Stand", 80)
vars.DragonsStand:AddTransition(1045, WorldPos(23063.6, 32378.2, -3138.05), WorldPos(-31545.2, -22737.3, -189.248))
vars.DragonsStand:AddWaypoint(2304, WorldPos(-1250.58, 17132.7, -3601.6))
vars.DragonsStand:AddWaypoint(2130, WorldPos(3507.99, 22758.6, -1815.75))
vars.DragonsStand:AddWaypoint(2131, WorldPos(12827.3, 3519.55, -4151.86))
vars.DragonsStand:AddWaypoint(2149, WorldPos(25683.7, 314.96, -1728.54))
vars.DragonsStand:AddWaypoint(2135, WorldPos(1552.96, -13742.6, -3878.16))
vars.DragonsStand:AddWaypoint(2170, WorldPos(29441.7, 23504.7, -2107.77))
vars.DragonsStand:AddWaypoint(2064, WorldPos(25558, 30539.5, -2799.56))
vars.DragonsStand:AddWaypoint(2094, WorldPos(14896.1, 12708.9, -2512.74))
vars.DragonsStand:AddWaypoint(2199, WorldPos(-3554.08, -5065.17, -4399.13))
vars.DragonsStand:AddWaypoint(2222, WorldPos(-19859.8, 3291.88, -3689.28))
vars.DragonsStand:AddWaypoint(2127, WorldPos(33663.7, 8663.73, -2630.34))

vars.AuricBasin = Map:new(1043, "Auric Basin", 80)
vars.AuricBasin:AddTransition(1045, WorldPos(20548.8, -30106.6, -8493.58), WorldPos(-31144.8, 6466.34, -10139.5))
vars.AuricBasin:AddTransition(1052, WorldPos(-17875.8, 31599.9, -19703.4), WorldPos(-30623.1, -15838, -14917.8))
vars.AuricBasin:AddWaypoint(2121, WorldPos(13443.4, -26519.4, -9276.57))
vars.AuricBasin:AddWaypoint(2050, WorldPos(700.401, -16327.1, -11146.4))
vars.AuricBasin:AddWaypoint(1990, WorldPos(346.455, -1107.73, -2858.37))
vars.AuricBasin:AddWaypoint(2156, WorldPos(17849.7, 2813.07, -11493.6))
vars.AuricBasin:AddWaypoint(2013, WorldPos(2528.06, 19315.7, -12938.1))
vars.AuricBasin:AddWaypoint(2006, WorldPos(-15159.2, 26389.1, -18767.6))
vars.AuricBasin:AddWaypoint(2054, WorldPos(-14289.6, 9338.59, -10646))

vars.TangledDepths = Map:new(1045, "Tangled Depths", 80)
vars.TangledDepths:AddTransition(1041, WorldPos(-31557.9, -22770.1, -191.018), WorldPos(23330.2, 32188.2, -3179.16))
vars.TangledDepths:AddTransition(1043, WorldPos(-31456.6, 6348.46, -10205.5), WorldPos(19772, -30256.3, -8623.74))
vars.TangledDepths:AddWaypoint(2048, WorldPos(23583.8, -13230.6, -9191.03))
vars.TangledDepths:AddWaypoint(2184, WorldPos(-32166.9, -21957.6, -236.677))
vars.TangledDepths:AddWaypoint(2051, WorldPos(17394.9, 5620.58, -32.8397))
vars.TangledDepths:AddWaypoint(1996, WorldPos(-7639.6, -7048.5, -8860.78))
vars.TangledDepths:AddWaypoint(2037, WorldPos(6108.91, -13717.7, -100.168))
vars.TangledDepths:AddWaypoint(2062, WorldPos(-27984.7, 8119.69, -9509.42))
vars.TangledDepths:AddWaypoint(2060, WorldPos(1138.06, 14868.3, -4944.03))

vars.VerdantBrink = Map:new(1052, "Verdant Brink", 80)
vars.VerdantBrink:AddTransition(1043, WorldPos(-31161.3, -16767.1, -14937.5), WorldPos(-17889.1, 31246.8, -19676.2))
vars.VerdantBrink:AddTransition(1015, WorldPos(37807.8, 2617.62, -16415.1), WorldPos(-16162.1, -12466.8, -16780.2))
vars.VerdantBrink:AddWaypoint(2016, WorldPos(28243, 15413.8, -14459.3))
vars.VerdantBrink:AddWaypoint(2049, WorldPos(14662.3, -15363.1, -17120.8))
vars.VerdantBrink:AddWaypoint(2056, WorldPos(5321.76, -994.742, -16906.5))
vars.VerdantBrink:AddWaypoint(2076, WorldPos(-30307.4, -15664.4, -14870.6))
vars.VerdantBrink:AddWaypoint(2005, WorldPos(-10618.7, 5766.73, -21362.5))
vars.VerdantBrink:AddWaypoint(2014, WorldPos(36856.5, 2001.21, -16458.7))
vars.VerdantBrink:AddWaypoint(2031, WorldPos(-19769.9, -10905.2, -20077.5))


mapTable:AddMap(vars.Queensdale)
mapTable:AddMap(vars.Hinterlands)
mapTable:AddMap(vars.DivinitysReach)
mapTable:AddMap(vars.Ashford)
mapTable:AddMap(vars.Blazeridge)
mapTable:AddMap(vars.FieldsOfRuin)
mapTable:AddMap(vars.Fireheart)
mapTable:AddMap(vars.Kessex)
mapTable:AddMap(vars.Gendarran)
mapTable:AddMap(vars.IronMarches)
mapTable:AddMap(vars.Dredgehaunt)
mapTable:AddMap(vars.LornarsPass)
mapTable:AddMap(vars.WayfarerFoothills)
mapTable:AddMap(vars.Timberline)
mapTable:AddMap(vars.Frostgorge)
mapTable:AddMap(vars.SnowdenDrifts)
mapTable:AddMap(vars.Diessa)
mapTable:AddMap(vars.Caledon)
mapTable:AddMap(vars.Metrica)
mapTable:AddMap(vars.Maelstrom)
mapTable:AddMap(vars.LionsArch)
mapTable:AddMap(vars.Straits)
mapTable:AddMap(vars.Sparkfly)
mapTable:AddMap(vars.Brisban)
mapTable:AddMap(vars.CursedShore)
mapTable:AddMap(vars.MalchorsLeap)
mapTable:AddMap(vars.Bloodtide)
mapTable:AddMap(vars.Grove)
mapTable:AddMap(vars.RataSum)
mapTable:AddMap(vars.BlackCitadel)
mapTable:AddMap(vars.Hoelbrak)
mapTable:AddMap(vars.SouthsunCove)
mapTable:AddMap(vars.DryTop)
mapTable:AddMap(vars.Silverwastes)
mapTable:AddMap(vars.DragonsStand)
mapTable:AddMap(vars.AuricBasin)
mapTable:AddMap(vars.TangledDepths)
mapTable:AddMap(vars.VerdantBrink)

-------------------------------------------------------


-- verify that every named map is also contained in the map table and export them:
for key, obj in pairs(vars) do
	if m_util.Inherits(obj, Map) and mapTable:HasMap(obj.id) == false then
		critical("Maps integrity is corrupted: Map \"" .. obj.name .. "\" is not contained in the map table")
	end

    export[key] = obj
end

local integrity = mapTable:CheckIntegrity()
if integrity ~= nil then
	critical("Maps integrity is corrupted: " .. integrity)
end

export.Waypoint = Waypoint
export.Map = Map
export.MapTable = MapTable

-- let the module behave like the map table:
setmetatable(export, {__index = mapTable})

return export