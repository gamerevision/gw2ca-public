--[[
This file can be used as a standard main.lua for FSM Bots.
Just include it in your main.lua and put the fsm of your Bot into
a file called bot.lua.

This module will start a new FSM for every Client that connects 
and create an encapsulating FSM that resets or restarts your bot when neccessary.
]]

local m_triggerService = require 'gw2/trigger_service.lua'
local m_moduleRegistry = require 'common/module.lua'
local m_fsm = require 'common/fsm.lua'
local m_time = require 'common/time.lua'
local m_maps = require 'gw2/maps.lua'

local g_stateMachines = {}
local g_fsmTrigger = {}
local g_connectedClients = {}
local g_args = {}

--[[
This function will wrap api events into fsm events.
@param mgr the name of the Manager that exports the event
@param event the name of the event to wrap
@return gw2/trigger_service.lua:Trigger
]]
function CreateFSMTrigger(mgr, event)    
    if g_fsmTrigger[event] ~= nil then return g_fsmTrigger[event] end
    local trigger = m_triggerService.Trigger:new(mgr, event, function(...)
            if g_stateMachines[Client:GetId()] then
                g_stateMachines[Client:GetId()]:Fire(event, ...) 
            end
        end)
    m_triggerService:AddTrigger(trigger)
    g_fsmTrigger[event] = trigger
    return trigger
end

--[[
Creates a top level FSM for the bot
]]
local function CreateFSM()
    local leave = m_fsm.State:new("WorldLeave")
    function leave:OnEnter(disc)
        information("Unexpected world leave. Flag: " .. tostring(disc))
        self.machine.Fire("Restart")
    end

    local finished = m_fsm.State:new("finished")
    function finished:OnEnter()
        information("Bot finished")
        Client:Kick()
    end
    
    local err = m_fsm.State:new("Error")
    function err:OnEnter(err)
        error(err)
        self.machine.Fire("Restart")
    end
    
    local restart = m_fsm.State:new("Restart")
    function restart:OnEnter()
        g_stateMachines[Client:GetId()]:Stop()
        if Client:Ingame() then
            Client:LogoutToCharselect()
        end
        
        -- destroy the current fsm for the client and create a new one
        local newfsm = CreateFSM()
        g_stateMachines[Client:GetId()] = nil
        
        -- wait 5 minutes and then start the new fsm
        information("Restarting in 5 minutes")
        Client:GetTimer():RegisterTrigger(function()
            g_stateMachines[Client:GetId()] = newfsm
            newfsm:Start()
        end, m_time.min(5), 0)
    end
    
    local runBot = m_fsm.State:new("RunBot", m_moduleRegistry:GetModule("Bot").CreateFSM(finished, err))
    
    local start = m_fsm.State:new("Start")
    function start:OnEnter()
        self.machine.Fire("Run", g_args)
    end
    
    local fsm = m_fsm.StateMachine:new({start, leave, runBot, restart, finished, err}, start, {
        {start, "Run", runBot},
        {runBot, "OnWorldLeave", leave},
        {leave, "Restart", restart},
        {err, "Restart", restart}
    })

    function fsm:OnStart()
      information("FSM started")
      Client:GetTimer():RegisterTrigger(self.machine.Step, 0, 40)
    end

    function fsm:OnStop()
      information("FSM stopped")
      Client:GetTimer():RemoveTrigger(self.machine.Step)
    end

    fsm.verbose = g_debug

    return fsm
end

--[[
Returns a list of Clients that are connected to the bot
]]
function GetConnectedClients()
    return g_connectedClients
end

--[[
Prints some usefull stats about the bot
]]
function Stats()
    for i, c in ipairs(g_connectedClients) do
        if c:GetEmail() ~= "" then
            information(c:GetEmail())
        else
            information("Client : " .. tostring(i))
        end
        information("\t" .. tostring(c:GetWalletMgr():GetGold()))
        if c:Ingame() then
            information("\t" .. tostring(c:GetAgentMgr():GetOwnAgent():GetPlayer():GetName() .. " : " ..  c:GetAgentMgr():GetOwnAgent():GetLevel()))
            information("\t" .. m_maps:GetMap(c:GetMapId()).name)
        else
            information("\tIn Charselect")
        end
    end
end

--[[
Returns the FSM for the current client
]]
function GetFSM()
    return g_stateMachines[Client:GetId()]
end

--[[
Returns the arguments that main was called with
]]
function GetArgs()
    return g_args
end

function init()
    if Client:GetEmail() ~= "" then
        information(Client:GetEmail() .. " connected to the bot")
    else
        information("New official Client connected to the bot")
    end
    
    m_triggerService:AddClient(Client)
    table.insert(g_connectedClients, Client)
    
    local fsm = CreateFSM()
    g_stateMachines[Client:GetId()] = fsm
    
    fsm:Start()
end

function main(args)
    g_args = args
    m_triggerService:AddTrigger(CreateFSMTrigger("client", "OnWorldLeave"))
end