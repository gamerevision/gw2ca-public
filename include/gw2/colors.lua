--[[
A module defining some sweet default color palette so you won't have to define them on your own.
The color palette has been created by mrmrs and can be found on: https://github.com/mrmrs/colors
]]

--[[
Given a color in either RGB or ARGB format this function returns the same color
with its opacity set to the given value. The returned value is always in ARGB format.
opacity shall be a float between 0 and 1; if omitted it defaults to 1.
]]
local function SetOpacity(color, opacity)
	if opacity == nil or opacity > 1 then
		opacity = 1
	elseif opacity < 0 then
		opacity = 0
	end

	local alpha = math.floor(255 * opacity) << 24
	return alpha | color
end

--[[
Given a color in RGB format this returns the same color in ARGB with an opacity of 1.
]]
local function ToARGB(color)
	return SetOpacity(color)
end

--[[
The MIT License (MIT)

Copyright (c) 2015 @mrmrs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Applies to the colors defined below:
]]
local colors =  {
	Navy = ToARGB(0x001F3F),
	Blue = ToARGB(0x0074D9),
	Aqua = ToARGB(0xFDBFF),
	Teal = ToARGB(0x39CCCC),
	Olive = ToARGB(0x3D9970),
	Green = ToARGB(0x2ECC40),
	Lime = ToARGB(0x01FF70),
	Yellow = ToARGB(0xFFDC00),
	Orange = ToARGB(0xFF851B),
	Red = ToARGB(0xFF4136),
	Maroon = ToARGB(0x85144B),
	Fuchsia = ToARGB(0xF012BE),
	Purple = ToARGB(0xB10DC9),
	Black = ToARGB(0x111111),
	Gray = ToARGB(0xAAAAAA),
	Silver = ToARGB(0xDDDDDD),
	White = ToARGB(0xFFFFFF),
}

return {
	SetOpacity = SetOpacity,
	ToARGB = ToARGB,

	Navy = colors.Navy,
	Blue = colors.Blue,
	Aqua = colors.Aqua,
	Teal = colors.Teal,
	Olive = colors.Olive,
	Green = colors.Green,
	Lime = colors.Lime,
	Yellow = colors.Yellow,
	Orange = colors.Orange,
	Red = colors.Red,
	Maroon = colors.Maroon,
	Fuchsia = colors.Fuchsia,
	Purple = colors.Purple,
	Black = colors.Black,
	Gray = colors.Gray,
	Silver = colors.Silver,
	White = colors.White,
}