local m_log = include("common/log.lua")

--[[
This module contains various utility functions which can either regard lua or gw2ca as well.
In the future the pure lua part of this module might be split into another file.
]]


--[[
Ordered table iterator, allow to iterate on the natural order of the keys of a
table.

Source from: http://lua-users.org/wiki/SortedIteration
]]
    local function __genOrderedIndex( t )
        local orderedIndex = {}
        for key in pairs(t) do
            table.insert( orderedIndex, key )
        end
        table.sort( orderedIndex )
        return orderedIndex
    end

    --[[
    Equivalent of the next function, but returns the keys in the alphabetic order.
    We use a temporary ordered key table that is stored in the table being iterated.
    ]]
    local function orderedNext(t, state)
        --print("orderedNext: state = "..tostring(state) )
        if state == nil then
            -- the first time, generate the index
            t.__orderedIndex = __genOrderedIndex( t )
            key = t.__orderedIndex[1]
            return key, t[key]
        end
        -- fetch the next value
        key = nil
        for i = 1, #t.__orderedIndex do
            if t.__orderedIndex[i] == state then
                key = t.__orderedIndex[i+1]
            end
        end

        if key then
            return key, t[key]
        end

        -- no more value to return, cleanup
        t.__orderedIndex = nil
        return
    end

    --[[
    Equivalent of the pairs() function on tables. Allows to iterate in order.
    ]]
    local function orderedPairs(t)
        --
        return orderedNext, t, nil
    end
--[[ END ]]

--[[
Reverses an array (sequential table) in place.
]]
local function ReverseArray(arr)
    assert(type(arr) == "table", "table expected")

    local i = 1
    local j = #arr
    local temp
    
    if j <= 0 then
        return
    end
    
    while i < j do
        temp = arr[i]
        arr[i] = arr[j]
        arr[j] = temp
        
        i = i + 1
        j = j - 1
    end
end

--[[
Given a number returns a string with the representation of the number in the given base.
base must be an integer between 2 and 36
]]
local itoaLockup = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}
local function itoa(number, base)
    assert(type(number) == "number", "first arg must be a number")
    assert(base % 1 == 0 and 2 <= base and base <= 36, "base must be an integer between 2 and 36")

    local str = {}
    local remainder
    
    repeat
        remainder = number % base
        number = (number-remainder) / base
        table.insert(str, itoaLockup[remainder+1])      
    until number==0
   
    ReverseArray(str)
    return table.concat(str)
end

--[[
Pads strings to the given length by prepending the given char.
If no char is given a whitespace character is used.
]]
local function LPadString(str, len, char)
    if char == nil then char = " " end
    
    assert(type(str) == "string", "str must be a string")
    assert(type(len) == "number" and len % 1 == 0, "len must be an integer")
    assert(type(char) == "string" and #char == 1, "char must be a single character")
    
    return string.rep(char, len - #str) .. str
end

--[[
Pads strings to the given length by appending the given char.
If no char is given a whitespace character is used.
]]
local function RPadString(str, len, char)
    if char == nil then char = " " end
    
    assert(type(str) == "string", "str must be a string")
    assert(type(len) == "number" and len % 1 == 0, "len must be an integer")
    assert(type(char) == "string" and #char == 1, "char must be a single character")
    
    return str .. string.rep(char, len - #str)
end

--[[
Walks up the class hierachy (metatables) of a table and checks whether it inherits a specific class.
]]
local function Inherits(obj, class)
    local parent = obj
    repeat
        parent = getmetatable(parent)
        if parent == class then
            return true
        end
    until parent == nil

    return false
end

--[[
Searches for the given `value` in `tbl` and returns its key or nil.
]]
local function Find(tbl, value)
    for k, v in pairs(tbl) do
        if v == value then
            return k
        end
    end

    return nil
end

--[[
Returns whether `tbl` contains given `value`.
]]
local function Contains(tbl, value)
    return Find(tbl, value) ~= nil
end



--[[
Returns the number of elements in a table.
Opposed to table.getn() this fucntion works with non-arrays as well.
]]
local function Count(obj)
    local count = 0
    for k,v in pairs(obj) do
        count = count + 1
    end

    return count
end

--[[
Returns an array with all keys of a table.
]]
local function GetKeys(obj)
    local keys = {}
    local i = 1
    
    for k, v in pairs(obj) do
        keys[i] = k
        i = i + 1
    end
    
    return keys
end

--[[
Utility function which returns either the current client if the passed argument is nil,
or the passed argument. Supposing ternary operations are available in lua,
this function is equivalent to: `return client == nil ? Client : client`

This function is intended to ease the writing of library functions
which accept only one client as argument.

Example:
    function GetPosition(client)
        client = util.GetClient(client)
        ...
    end

    GetPosition() -> invokes the method on the current client
    GetPosition(clients.Foo) -> invokes the method on whatever client is stored in clients.Foo
]]
local function GetClient(client)
    if client == nil then
        return Client
    else
        return client
    end
end


local _ENV_PROXY_MT = {

    __index = function(tbl, key)
        local client = rawget(tbl, "client")
        local weakTable = rawget(tbl, "weakTable")
        assert(client ~= nil, "client == nil: Proxy _ENV returned by ExportManagers() has been corrupted")
        assert(weakTable ~= nil, "weakTable == nil: Proxy _ENV returned by ExportManagers() has been corrupted")
        
        local env = weakTable["env"]
        assert(env ~= nil, "env == nil: Proxy _ENV returned by ExportManagers() has been corrupted")
        
        local val = rawget(tbl, key)
        if val then
            return val
        end

        if key == "timer" then val = client:GetTimer(); rawset(tbl, "timer", val); return val
        elseif key == "agentMgr" then val = client:GetAgentMgr(); rawset(tbl, "agentMgr", val); return val
        elseif key == "chatMgr" then val = client:GetChatMgr(); rawset(tbl, "chatMgr", val); return val
        elseif key == "itemMgr" then val = client:GetItemMgr(); rawset(tbl, "itemMgr", val); return val
        elseif key == "walletMgr" then val = client:GetWalletMgr(); rawset(tbl, "walletMgr", val); return val
        elseif key == "lootMgr" then val = client:GetLootMgr(); rawset(tbl, "lootMgr", val); return val
        elseif key == "venMgr" then  val = client:GetVendorMgr(); rawset(tbl, "vendorMgr", val); return val
        elseif key == "dialogMgr" then val = client:GetDialogMgr(); rawset(tbl, "dialogMgr", val); return val
        elseif key == "questMgr" then val = client:GetQuestMgr(); rawset(tbl, "questMgr", val); return val
        elseif key == "taskMgr" then val = client:GetTaskMgr(); rawset(tbl, "taskMgr", val); return val
        elseif key == "tpMgr" then val = client:GetTradingPostMgr(); rawset(tbl, "tpMgr", val); return val
        elseif key == "mailMgr" then val = client:GetMailMgr(); rawset(tbl, "mailMgr", val); return val
        elseif key == "craftMgr" then val = client:GetCraftMgr(); rawset(tbl, "craftMgr", val); return val
        elseif key == "objMgr" then val = client:GetObjectiveMgr(); rawset(tbl, "objMgr", val); return val
        elseif key == "eventMgr" then val = client:GetEventMgr(); rawset(tbl, "eventMgr", val); return val
        elseif key == "navMgr" then val = client:GetNavigationMgr(); rawset(tbl, "navMgr", val); return val
        elseif key == "poiMgr" then val = client:GetPoiMgr(); rawset(tbl, "poiMgr", val); return val
        elseif key == "dbgMgr" then val = client:GetDebugMgr(); rawset(tbl, "dbgMgr", val); return val
        elseif key == "playerMgr" then val = client:GetPlayerMgr(); rawset(tbl, "playerMgr", val); return val
        elseif key == "debugMgr" then val = client:GetDebugMgr(); rawset(tbl, "debugMgr", val); return val
        else
            return env[key]
        end
    end,
    
    __newindex = function(tbl, key, value)
        local weakTable = rawget(tbl, "weakTable")
        assert(weakTable ~= nil, "weakTable == nil: Proxy _ENV returned by ExportManagers() has been corrupted")
    
        local env = weakTable["env"]
        assert(env ~= nil, "env == nil: Proxy _ENV returned by ExportManagers() has been corrupted")
    
        env[key] = value
    end
}

--[[
This function returns an expanded environment in which all managers of the given client are included.
You still have to replace your environment (_ENV) with the one returned by this function.

The first parameter is the environment to expand (usually the current _ENV) and the second is
the client from which to export the manager. If yonder parameter is nil, the currently active client is used.

IMPORTANT: Never forget the local before shadowing _ENV, otherwise you are replacing the chunk-scoped environment.

Example:
    function GetPosition(client)
        local _ENV = ExportManagers(_ENV, client)
        return agentMgr:GetOwnAgent():GetPosition()
    end

    function Foo(client1, client2)
        do local _ENV = ExportManagers(_ENV, client1)
            local inv = itemMgr:GetInventory() -- inventory of client1
        end

        do local _ENV = ExportManagers(_ENV, client2)
            local inv = itemMgr:GetInventory() -- inventory of client2
        end
    end

The manager are exposed with the following names:
    timer = client:GetTimer()
    agentMgr = client:GetAgentMgr()
    chatMgr = client:GetChatMgr()
    itemMgr = client:GetItemMgr()
    walletMgr = client:GetWalletMgr()
    lootMgr = client:GetLootMgr()
    venMgr = client:GetVendorMgr()
    dialogMgr = client:GetDialogMgr()
    questMgr = client:GetQuestMgr()
    taskMgr = client:GetTaskMgr()
    tpMgr = client:GetTradingPostMgr()
    mailMgr = client:GetMailMgr()
    craftMgr = client:GetCraftMgr()
    objMgr = client:GetObjectiveMgr()
    eventMgr = client:GetEventMgr()
    navMgr = client:GetNavigationMgr()
    effectMgr = client:GetEffectMgr()
    poiMgr = client:GetPoiMgr()
    dbgMgr = client:GetDebugMgr()
    playerMgr = client:GetPlayerMgr()
    drawMgr = client:GetDrawMgr()
]]
local function ExportManagers(env, client)
    client = GetClient(client)

    local weakTable = setmetatable({}, {__mode = "v"})
    weakTable["env"] = env
    
    local newEnv = setmetatable({}, _ENV_PROXY_MT)
    rawset(newEnv, "client", client)
    rawset(newEnv, "weakTable", weakTable)

    return newEnv, client
end

--[[
A list which can hold multiple callbacks.
]]
local Callbacks = {}
Callbacks.__index = Callbacks

    --[[
    Creates a new Callbacks object.
    ]]
    function Callbacks:new()
        local obj = setmetatable({}, self)
        obj.__index = obj
        
        obj.callbacks = {}

        return obj
    end

    --[[
    Returns whether the given callback is regitered.
    ]]
    function Callbacks:Has(callback)
        for i, cb in ipairs(self.callbacks) do
            if cb == callback then
                return true
            end
        end

        return false
    end

    --[[
    Adds a callback to this Callbacks object.
    Returns a function which if invoked removes the callback again.
    This is useful if you need to remove anonymous callbacks.

    You can't register the same callback twice. In that case this function will return nil.
    ]]
    function Callbacks:Add(callback)
        if self:Has(callback) then
            return
        end

        table.insert(self.callbacks, callback)

        -- Proxy functions might survive the lifetime of a Callbacks object,
        -- but we shouldn't keep it alive if the only reference to it is a "Remove" function
        local weakTable = setmetatable({}, {__mode = "v"})
        weakTable.self = self
        weakTable.callback = callback

        local function RemoveProxy()
            if weakTable.self == nil then
                error("Tried to call proxy method returned by call to Callbacks.Remove() but the original Callbacks object is already gone")
                return
            end

            return weakTable.self:Remove(weakTable.callback)
        end

        return RemoveProxy
    end

    --[[
    Removes a callback and returns whether one has really been removed.
    ]]
    function Callbacks:Remove(callback)
        for i, cb in ipairs(self.callbacks) do
            if cb == callback then
                table.remove(self.callbacks, i)
                return true
            end
        end

        return false
    end

    --[[
    Fires all callbacks and forwards all given parameters to them.
    Callbacks are fired in order of registration.
    ]]
    function Callbacks:Fire(...)
        for i, callback in self.callbacks do
            callback(...)
        end
    end
--[[ END ]]

--[[
Computes the euclidean distance between two points.
]]
local function ComputeDistance(pt1, pt2)
    return math.sqrt((pt1.x - pt2.x)^2 + (pt1.y - pt2.y)^2 + (pt1.z - pt2.z)^2)
end

--[[
Returns whether two points are in the passed range to each other.
]]
local function InRange(pt1, pt2, range)
    local sqDist = (pt1.x - pt2.x) ^ 2 + (pt1.y - pt2.y) ^ 2 + (pt1.z - pt2.z) ^ 2
    return sqDist <= range ^ 2
end

local function GetOwnAgent(client)
    local _ENV, client = ExportManagers(_ENV, client)
    return agentMgr:GetOwnAgent()
end

--[[
Returns the position of the given client (can be nil).
This is a shortcut for "Client:GetAgentMgr():GetOwnAgent():GetPosition()".
]]
local function GetPosition(client)
    local _ENV, client = ExportManagers(_ENV, client)
    return agentMgr:GetOwnAgent():GetPosition()
end

--[[
Returns the agent closest to the given agent or nil if there isn't any in the network bubble.
If maxRange is given, agents farer away than maxRange are excluded.
]]
local function GetClosestAgentToAgent(client, agent, maxRange)
    local _ENV, client = ExportManagers(_ENV, client)
    
    assert(agent ~= nil, "agent must not be nil")
    
    if maxRange == nil then
        maxRange = 0
    end
    
    local pos = agent:GetPosition()
    
    local closest = nil
    local bestDistance = nil
    
    for id, ag in agentMgr:GetAgents() do
        local distance = ComputeDistance(pos, ag:GetPosition())
        if ag:GetAgentId() ~= agent:GetAgentId() and (closest == nil or distance < bestDistance) then
            closest = ag
            bestDistance = distance
        end
    end
    
    if maxRange ~= 0 and bestDistance > maxRange then
        return nil
    else
        return closest
    end
end


--[[
Wrapper for SetTarget, OnTargetReached and OnAbort
]]
local function MoveTo(client, pos, endDist, maxDist, OnReached, OnAbort)
    local _ENV, client = ExportManagers(_ENV, client)

    local lastPos = agentMgr:GetOwnAgent():GetPosition()
	local cleared = false
	
	local function Check()
		if cleared then
			m_log.debug("Already cleared")
			return
		end
	
        if not agentMgr:GetOwnAgent():IsAlive() then
            clear()
            m_log.warning("we died while moving")
            OnReached()
        end
        local pos = agentMgr:GetOwnAgent():GetPosition()
        if ComputeDistance(lastPos, pos) < 5 then
            m_log.error("failed to move")
            assert(navMgr:SetTarget(pos, endDist, maxDist))
        end
        lastPos = pos
    end
	
    local function clear()
		cleared = true
	
        navMgr:ClearEvent(NavigationMgr.OnAbort)
        navMgr:ClearEvent(NavigationMgr.OnStopped)
        navMgr:ClearEvent(NavigationMgr.OnTargetReached)
        timer:RemoveTrigger(Check)
    end

    local ReachedHandler = function()
        clear()
        OnReached()
    end

    local AbortHandler = function()
        clear()
        m_log.warning("aborted")
        if OnAbort ~= nil then OnAbort() end
    end

    local StoppedHandler = function()
        m_log.warning("stopped")
        assert(MoveTo(client, pos, endDist, maxDist, OnReached, OnAbort), "MoveTo Stopped no way to continue")
    end

    navMgr:RegisterSingleshot(NavigationMgr.OnTargetReached, ReachedHandler)
    navMgr:RegisterSingleshot(NavigationMgr.OnAbort, AbortHandler)
    navMgr:RegisterSingleshot(NavigationMgr.OnStopped, StoppedHandler)
    timer:RegisterTrigger(Check, 1000, 1000)

    if navMgr:SetTarget(pos, endDist, maxDist) then
        return true
    end
    
    clear()
    return false
end

--[[
Logs a very long string message using the given logFunc by smartly dividing it into large chunks.
logFunc is supposed to be any of the logging functions (information, notice, warning etc.) but can actually be any function accepting a single string as argument (e.g print).
splitAtSpaces is optional and defaults to false.

Log messages are restricted to a certain size, approximately 16kb. For some cases this might not be sufficient, and the message has to split
into multiple smaller ones. This algorithm tries to smartly split the message into chunks with a size of at least 12kb and at most 16kb.
The algorithm tries to split at newlines by default and at newline & spaces if the splitAtSpaces flag is set to true.
If anywhere in that 4kb buffer such a delimiter is detected, the message is instantly split. If however none could be found,
the message is hard split at whichever character is unfortunate enough to be at the 16kb position.
Given that the log message has enough newlines and spaces, even if they are only occurring every 1000 characters, this function should still produce nice outputs.

Example:
    lcoal tlotr = "a book sure has many characters ..."
    LogLongMessage(information, tlotr)
    
TODO: concat operator (..) is inperformant, switch to table.concat
]]
local function LogLongMessage(logFunc, message, splitAtSpaces)
    assert(logFunc ~= nil, "logFunc must not be nil")
    assert(message ~= nil, "message must not be nil")
    
    local pattern
    if splitAtSpaces then
        pattern = "([^ \n]+)([ \n]*)"
    else
        pattern = "([^\n]+)(\n*)"
    end
    
    -- constants for the algorithm:
    local minimum = 12000
    local maximum = 16000
    
    local chunks = {}
    local size = 0
    local lastDeliLength = 0
    
    table.insert(chunks, "")
    
    for word, delimiter in string.gmatch(message, pattern) do
        local i = #chunks
        local deliLength = string.len(delimiter)
        local length = string.len(word) + deliLength
        
        if size + length > maximum then
        
            if size - lastDeliLength >= minimum then
                -- we already got enough in the current chunk, lets start a new one
                table.insert(chunks, "")
                size = 0
                
                if lastDeliLength ~= 0 then
                    -- remove the last delimiter
                    chunks[i] = string.sub(chunks[i], 1, -lastDeliLength - 1)
                end
            end
    
            local remaining = length
            local pos = 1
            
            repeat
                local j = #chunks
            
                if size + remaining <= maximum then
                    local tail = string.sub(word, pos)
                    
                    chunks[j] = chunks[j] .. tail .. delimiter
                    size = size + string.len(tail) +  deliLength
                    
                    break
                end
                
                local substr = string.sub(word, pos, pos + maximum - size - 1)
                chunks[j] = chunks[j] .. substr
                
                pos = pos + string.len(substr)
                remaining = remaining - string.len(substr)
                
                table.insert(chunks, "")
                size = 0
            until pos >= length
            
        else
            chunks[i] = chunks[i] .. word .. delimiter
            size = size + length
        end
        
        -- store deliLength so we can remove the delimiter again if needed
        lastDeliLength = deliLength
    end
    
    for i, chunk in ipairs(chunks) do
        logFunc(chunk)
    end
end


return {
    orderedPairs = orderedPairs,
    ReverseArray = ReverseArray,
    itoa = itoa,
    LPadString = LPadString,
    RPadString = RPadString,
    Inherits = Inherits,
    Find = Find,
    Contains = Contains,
    Count = Count,
    GetKeys = GetKeys,
    GetClient = GetClient,
    ExportManagers = ExportManagers,
    Callbacks = Callbacks,
    ComputeDistance = ComputeDistance,
    InRange = InRange,
    GetOwnAgent = GetOwnAgent,
	GetPosition = GetPosition,
    GetClosestAgentToAgent = GetClosestAgentToAgent,
    MoveTo = MoveTo,
    LogLongMessage = LogLongMessage
}