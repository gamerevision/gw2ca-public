

-- Skill(skillslot, name, castTime, cooldown, range, isAoe)
local Longbow =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Long Range Shot", 750, 0, 1200, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Rapid Fire", 2500, 10000, 1200, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Hunters Shot", 0, 12000, 1200, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Point Blank Shot", 500, 15000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Barrage", 2250, 30000, 1200, true),
}

local Shortbow =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Crossfire", 0, 0, 900, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Poison Volley", 250, 9000, 900, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Quick Shot", 250, 9000, 900, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Crippling Shot", 0, 12000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Concussion Shot", 250, 25000, 1200, true),
}

local Greatsword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Slash", 500, 0, 150, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Maul", 750, 6000, 220, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Swoop", 1750, 12000, 1100, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Counterattack", 3000, 15000, 150, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Hit Bash", 500, 25000, 300, false),
}

local Sword =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Slash", 500, 0, 130, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Hornet Sting", 500, 8000, 130, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Serpents Strike", 1000, 15000, 130, false),
}

local Axe =
{
  ["Weapon1"] = Skill(GW2.SkillSlot.Weapon1, "Ricochet", 250, 0, 900, false),
  ["Weapon2"] = Skill(GW2.SkillSlot.Weapon2, "Splitblade", 750, 6000, 900, false),
  ["Weapon3"] = Skill(GW2.SkillSlot.Weapon3, "Winters Bite", 500, 10000, 900, false),
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Path of Scars", 500, 15000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Whirling Defense", 5000, 25000, 0, false),
}

local Torch =
{
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Throw Torch", 500, 15000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Bonfire", 500, 25000, 120, false),
}

local Warhorn =
{
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Hunters Call", 1500, 25000, 900, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Call of the Wild", 500, 35000, 600, false),
}

local Dagger =
{
  ["Weapon4"] = Skill(GW2.SkillSlot.Weapon4, "Stalkers Strike", 500, 10000, 250, false),
  ["Weapon5"] = Skill(GW2.SkillSlot.Weapon5, "Crippling Talon", 500, 15000, 900, false),
}

local WeaponSkills =
{
  [GW2.WeaponType.Sword] = Sword,
  --[GW2.WeaponType.Hammer] = Hammer,
  [GW2.WeaponType.Longbow] = Longbow,
  [GW2.WeaponType.Shortbow] = Shortbow,
  [GW2.WeaponType.Axe] = Axe,
  [GW2.WeaponType.Dagger] = Dagger,
  [GW2.WeaponType.Greatsword] = Greatsword,
  --[GW2.WeaponType.Mace] = Mace,
  --[GW2.WeaponType.Pistol] = Pistol,
  --[GW2.WeaponType.Polearm] = Polearm,
  --[GW2.WeaponType.Rifle] = Rifle,
  --[GW2.WeaponType.Scepter] = Scepter,
  --[GW2.WeaponType.Staff] = Staff,
  --[GW2.WeaponType.Focus] = Focus,
  [GW2.WeaponType.Torch] = Torch,  
  [GW2.WeaponType.Warhorn] = Warhorn,
  --[GW2.WeaponType.Shield] = Shield,  
}

return 
{
  WeaponSkills = WeaponSkills,
}
