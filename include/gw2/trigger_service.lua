local m_util = require 'gw2/util.lua'
local setmetatable = setmetatable
local pairs = pairs

--[[
Utility class for the TriggerService
]]
local Trigger = {}
Trigger.__index = Trigger

--[[
Creates a new Trigger
@param mgr the name of the manager that exports the event @see gw2/util.lua:ExportManagers
@param event the name of the event
@param func the function to call if the event fires
]]
function Trigger:new(mgr, event, func)
    local obj = setmetatable({}, self)
    obj.__index = obj
    
    obj.mgr = mgr
    obj.event = event
    obj.func = function(...) func(...) end
    
    return obj
end

--[[
Registers the Trigger to the given Client
@param client the client to register to
]]
function Trigger:Register(client)
    local _ENV = m_util.ExportManagers(_ENV, client)
    local mgr = _ENV[self.mgr]
    mgr:RegisterTrigger(mgr[self.event], self.func)
end

--[[
Removes the Trigger from the given Client
@param client the client to remove from
]]
function Trigger:Remove(client)
    local _ENV = ExportManagers(_ENV, client)
    local mgr = _ENV[self.mgr]
    mgr:RemoveTrigger(mgr[self.event], self.func)
end

-----------------------------------------

--[[
Holds a list of Triggers to register to all Clients
]]
local TriggerService = {}
TriggerService.__index = TriggerService

--[[
Adds a Trigger. 
The Trigger will be registered to all Clients added to the Service
@param t the Trigger to add
]]
function TriggerService:AddTrigger(t)
    local list = self.triggerList[t.event] or {}
    local found = false
    for _, t2 in pairs(list) do
        if t.func == t2.func then
            found = true
            break
        end
    end
    
    if not found then table.insert(list, t) end
    self.triggerList[t.event] = list
    
    for _, c in pairs(self.clientList) do
        t:Register(c)
    end
end

--[[
Removes a Trigger.
The Trigger will be removed from all Clients added to the Service
@param t the Trigger to remove
]]
function TriggerService:RemoveTrigger(t)
    --[[
    local event = nil
    if type(t) == "string" then event = t end
    if type(t) == "table" then event = t.event end
    assert(event ~= nil, "Invalid argument")
    local trig = assert(self.triggerList[event], "Tried to remove nonexisting trigger")
    for _, c in pairs(self.clientList) do
        trig:Remove(c)
    end
    self.triggerList[event] = nil]]
end

--[[
Adds a Client to the Service.
All Triggers that are already added will be registered to the Client
@param c the Client to add
]]
function TriggerService:AddClient(c)
    self.clientList[c:GetId()] = c
    for _, list in pairs(self.triggerList) do
        for _, t in ipairs(list) do
            for _, c in pairs(self.clientList) do
                t:Register(c)
            end
        end
    end
    
end

local function CreateTriggerService()
    if s_triggerService ~= nil then return s_triggerService end
    
    local obj = setmetatable({}, TriggerService)
    obj.__index = obj
    
    obj.triggerList = {}
    obj.clientList = {}
    
    return obj
end

s_triggerService = CreateTriggerService()

local m = setmetatable({}, {__index = s_triggerService})
m.Trigger = Trigger
return m