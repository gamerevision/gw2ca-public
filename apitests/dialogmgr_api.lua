--[[
    (C) Copyright 2015, Recode-Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- DialogMgr, Dialog
-- Preparation:
--   - start script
--   - travel to Hall of Monuments
-------------------------------------------------------------------------------
include "static_test_data.lua"
include "misc.lua"

OnDisplayDialog_called = false
OnDisplayDialog_counter = 0
function OnDisplayDialog(dialog)
    -- we nee this variables in our test functions
    dialog_mullenix = dialog
    assert(dialog_mullenix, "dialog_mullenix is nil")
    
    OnDisplayDialog_called = true
    OnDisplayDialog_counter = OnDisplayDialog_counter + 1
end

OnDialogClosed_called = false
function OnDialogClosed()
    OnDialogClosed_called = true
end

-------------------------------------------------------------------------------
function WaitInteract()
    Test_DialogMgr_OnDisplayDialog()
    Test_Dialog_GetPageId()
    Test_Dialog_GetOptionCount()
    Test_Dialog_GetOptions()
    
    assert(dialog_mullenix, "dialog_mullenix is nil")

    -- Prepare Test_Dialog_Select
    local options = dialog_mullenix:GetOptions()
    assert(options:size() == MULLENIX_DIALOG_OPT_COUNT)

    dlg_continue = options:at(0)
    dialog_mullenix:Select(dlg_continue)    
    
    -- We can test only Select, because all other Select* functions
    -- are wrappers around Select.  
    Test_Dialog_Select()
    
    -- Prepare Test_DialogMgr_OnDialogClosed
    dialog_mullenix:ExitDialog()
    Client:GetTimer():RegisterTrigger(WaitExitDialog, 3000, 0)
end

function WaitExitDialog()
    Test_DialogMgr_OnDialogClosed()
    assert(false, "TESTS DONE")
end

-------------------------------------------------------------------------------
-- DialogMgr events
-------------------------------------------------------------------------------
function Test_DialogMgr_OnDisplayDialog()
    assert(OnDisplayDialog_called, "OnDisplayDialog never called")
end

function Test_DialogMgr_OnDialogClosed()
    assert(OnDialogClosed_called, "OnDialogClosed never called")
end

-------------------------------------------------------------------------------
-- Dialog functions
-------------------------------------------------------------------------------
function Test_Dialog_GetPageId()
    assert(dialog_mullenix, "dialog_mullenix is nil")
    assert(dialog_mullenix:GetPageId() == MULLENIX_DIALOG_FIRST_PAGE_ID)
end

function Test_Dialog_GetOptionCount()
    assert(dialog_mullenix, "dialog_mullenix is nil")
    assert(dialog_mullenix:GetOptionCount() == MULLENIX_DIALOG_OPT_COUNT)
end

function Test_Dialog_GetOptions()
    assert(dialog_mullenix, "dialog_mullenix is nil")
    
    local options = dialog_mullenix:GetOptions()
    assert(options, "options is nil")
    assert(options:size() == MULLENIX_DIALOG_OPT_COUNT, "wrong options size")

    -- check first option
    assert(options:at(0).id == 0, "option id is not 0")
    assert(options:at(0).type == Dialog.OptionType.Continue, "wrong type")
    
    -- check second option
    assert(options:at(1).id == 1, "option id is not 1")
    assert(options:at(1).type == Dialog.OptionType.Exit, "wrong type")
end

function Test_Dialog_Select()
    -- We called the Continue dialog option. So the OnDisplayDialog event
    -- will be called one more time. Check the counter here.
    assert(OnDisplayDialog_counter == 1)
end
-------------------------------------------------------------------------------

function OnWorldReveal()
    local mapid = Client:GetMapId()
    local navMgr = Client:GetNavigationMgr()
    
    -- check if we are on the right map
    if (mapid ~= HOM_MAPID) then
        print("ERROR: Wrong map. Travel to Hall of Monuments.")
        return
    end

    MaxTestDuration(60*1000, 0)
    
    -- move to the test position
    result = navMgr:SetTarget(MULLENIX_POS, 0, 0)
    assert(result, "navMgr:SetTarget failed")
end

function OnWorldLeave()
    -- reset test variables
    OnDisplayDialog_called = false
    OnDialogClosed_called = false
    OnDisplayDialog_counter = 0
end

function OnTargetReached()
    local cchar = Client:GetControlledCharacter()
    local agentMgr = Client:GetAgentMgr()
    local tmr = Client:GetTimer()
    
    -- Artificer Mullenix
    agent = agentMgr:GetAgentByName(MULLENIX_NAME)
    assert(agent, "agentMgr:GetAgentByName failed")

    -- interact with mullenix and then wait 3 seconds
    cchar:Interact(agent)
    tmr:RegisterTrigger(WaitInteract, 3000, 0)
end

function init()
    print("############################################################")
    print("# API test script initialized")
    print("# This script will test all DialogMgr and Dialog functions.")
    print("# To start this script, travel to Hall of Monuments.")
    print("############################################################")
    
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
    Client:RegisterTrigger(Gw2Client.OnWorldLeave, OnWorldLeave)
    Client:GetDialogMgr():RegisterTrigger(DialogMgr.OnDisplayDialog,OnDisplayDialog)
    Client:GetDialogMgr():RegisterTrigger(DialogMgr.OnDialogClosed, OnDialogClosed)
    Client:GetNavigationMgr():RegisterTrigger(NavigationMgr.OnTargetReached, OnTargetReached)
end