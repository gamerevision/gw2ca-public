--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- PoiMgr
-- Preparation:
--   - start script
--   - travel to one of the following maps: Divinity's Reach,
--     Black Citadel, Hoelbrak
-------------------------------------------------------------------------------

include "static_test_data.lua"
include "misc.lua"

-------------------------------------------------------------------------------
-- PoiMgr functions
-------------------------------------------------------------------------------
function Test_PoiMgr_GetWaypoints()
    local mapid = Client:GetMapId()
    local poiMgr = Client:GetPoiMgr()
    local pois = poiMgr:GetWaypoints()
    local n = pois:size()

    for i, d in pairs(WP_DATA) do
        if d.mapid == mapid and d.nwps == n then
            return
        end
    end

    assert(false, "No map tested")
end

-------------------------------------------------------------------------------

function Prepare_Test_PoiMgr_UseWaypoint()
    local mapid = Client:GetMapId()
    local poiMgr = Client:GetPoiMgr()
    local pois = poiMgr:GetWaypoints()
    local validmap = false
    
    for i, d in pairs(WP_DATA) do
        if d.mapid == mapid then
            validmap = true
        end
    end

    assert(validmap, "invalid map")  

    -- Get first unlocked Waypoint
    for i in pois do
        if i.locked == false then
            wp = i
            break
        end
    end
    
    result = poiMgr:UseWaypoint(wp)
    -- This should never happen
    assert(result == PoiMgr.WaypointError.Success, "UseWaypoint failed")
end

function Test_PoiMgr_UseWaypoint()
    -- Check if we are on the right position
    local agent = Client:GetAgentMgr():GetOwnAgent()
    assert(agent, "GetOwnAgent() failed")
    
    local pos = agent:GetPosition()
    assert(distance(pos, wp.pos) < 1.0, "Position check failed")
end

-------------------------------------------------------------------------------
function WaitWP()
    Test_PoiMgr_UseWaypoint()
    assert(false, "TESTS DONE")
end

once = false
function OnWorldReveal()
    if once == false then
        MaxTestDuration(20000)
        
        Test_PoiMgr_GetWaypoints()
        Prepare_Test_PoiMgr_UseWaypoint()
        Client:GetTimer():RegisterTrigger(WaitWP, 2000, 0)
        once = true
    end
end

function init()
    debug("############################################################")
    debug("# API test script initialized")
    debug("# This script will test most of the PoiMgr functions")
    debug("# To start this script, travel to one of the")
    debug("# following maps: Divinity's Reach, Black Citadel, Hoelbrak,")
    debug("# Rata Sum")
    debug("############################################################")
    
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
end