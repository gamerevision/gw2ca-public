--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- AgentMgr, AgentBase
-- Preparation:
--   - start script
--   - travel to Hall of Monuments
-- TODOs
--   - implement following tests:
--     Test_AgentBase_GetVelocity
--     Test_AgentMgr_GetClosestAgentToPoint_Attitude
--     Test_AgentMgr_GetAgentsByFilter
-------------------------------------------------------------------------------

include "static_test_data.lua"
include "misc.lua"

-------------------------------------------------------------------------------
-- CONFIG
-------------------------------------------------------------------------------
DELAY_FIRE_KILL     = 25000
DELAY_USEWP         = 5000

-------------------------------------------------------------------------------
-- AgentMgr functions
-------------------------------------------------------------------------------
function Test_AgentMgr_GetAgent()
    local agentMgr = Client:GetAgentMgr()
    -- Artificer Mullenix (id=13)
    local agent = agentMgr:GetAgent(MULLENIX_ID)
    assert(agent, "agentMgr:GetAgent failed")

    -- verify by name
    local name = agent:GetName()
    assert(name == MULLENIX_NAME, "agent name check failed")
end

function Test_AgentMgr_GetOwnId()
    local agentMgr = Client:GetAgentMgr()
    local myid = agentMgr:GetOwnId()
    assert(myid == OWN_ID, "agentMgr:GetOwnId failed")
end

function Test_AgentMgr_GetOwnAgent()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetOwnAgent()
    assert(agent, "agentMgr:GetOwnAgent failed")

    -- verify by id
    local agentid = agent:GetAgentId()
    assert(agentid == OWN_ID, "agent id check failed")
end

function Test_AgentMgr_GetAgentByPosition()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetAgent(MULLENIX_ID)
    assert(agent, "agentMgr:GetAgent failed")

    local pos = agent:GetPosition()
    local agent2 = agentMgr:GetAgentByPosition(pos)
    assert(agent2, "agentMgr:GetAgentByPosition failed")

    -- verify by id
    local agent2id = agent2:GetAgentId()
    assert(agent2id == MULLENIX_ID, "agent id check failed")
end

function Test_AgentMgr_GetAgentBySpeciesId()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetAgent(MULLENIX_ID)
    assert(agent, "agentMgr:GetAgent failed")

    local agent2 = agentMgr:GetAgentBySpeciesId(MULLENIX_SPECIESID)
    assert(agent2, "agentMgr:GetAgentBySpeciesId failed")

    -- verify by id
    local agentid = agent:GetAgentId()
    local agent2id = agent2:GetAgentId()
    assert(agent2id == agentid, "agent id check failed")
end

function Test_AgentMgr_GetAgentByName()
    local agentMgr = Client:GetAgentMgr()
    -- Artificer Mullenix (id=13)
    local agent = agentMgr:GetAgentByName(MULLENIX_NAME)
    assert(agent, "agentMgr:GetAgentByName failed")

    -- verify by id
    local agentid = agent:GetAgentId()
    assert(agentid == MULLENIX_ID, "agent id check failed")
end

function Test_AgentMgr_GetClosestAgentToPoint()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetClosestAgentToPoint(MULLENIX_POS)
    assert(agent, "agentMgr:GetClosestAgentToPoint failed")
    
    -- verify by id
    local agentid = agent:GetAgentId()
    assert(agentid == MULLENIX_ID, "agent id check failed")
end

function Test_AgentMgr_GetClosestAgentToPoint_Attitude()
    -- TODO
    assert(false, "Test_AgentMgr_GetClosestAgentToPoint_Attitude")
end

function Test_AgentMgr_GetAgentsByFilter()
    -- TODO
    assert(false, "Test_AgentMgr_GetAgentsByFilter")
end

function Test_AgentMgr_GetAgentCount()
    local agentMgr = Client:GetAgentMgr()
    local n = agentMgr:GetAgentCount()
    assert(n == HOM_AGENT_COUNT, "agentMgr:GetAgentCount failed")
end

function Test_AgentMgr_GetAgents()
    local agentMgr = Client:GetAgentMgr()
    local agents = agentMgr:GetAgents()
    
    assert(agents, "agentMgr:GetAgents failed")
    assert(agents:size() == HOM_AGENT_COUNT, "GetAgents: wrong size")
    
    -- check one agent by agentid and name, mullenix
    local mullenix = agents:at(MULLENIX_ID)
	assert(mullenix:GetName() == MULLENIX_NAME, "mullenix:GetName failed")
end

function Test_AgentMgr_Empty()
    local agentMgr = Client:GetAgentMgr()
    -- TODO: this is a bad test
    assert(agentMgr:Empty() == false, "agentMgr:Empty failed")
end

function Test_AgentMgr_OnAgentSpawned()
    -- check if OnAgentSpawned has been called 6 times
    assert(OnAgentSpawned_counter == HOM_AGENT_COUNT)
end

function Prepare_Test_AgentMgr_OnAgentDespawned()
    local navMgr = Client:GetNavigationMgr()
    local result = navMgr:SetTarget(TEST_POS, 0, 0)
    assert(result, "navMgr:SetTarget failed")

    navMgr:RegisterTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Despawned)
end

function Test_AgentMgr_OnAgentDespawned()
    -- check if OnAgentDespawned has been called 2 times
    assert(OnAgentDespawned_counter == 2)
end

function Prepare_Test_AgentMgr_OnAgentHealthChange()
    local agentMgr = Client:GetAgentMgr()
    agentMgr:RegisterTrigger(AgentMgr.OnAgentHealthChange, OnAgentHealthChange)
end

function Test_AgentMgr_OnAgentHealthChange()
    local agentMgr = Client:GetAgentMgr()
    local ownid = agentMgr:GetOwnId()
    
    assert(OnAgentHealthChange_data.agent, "OnAgentHealthChange failed")
    local agentid = OnAgentHealthChange_data.agent:GetAgentId()
    local srcagentid = OnAgentHealthChange_data.srcagent:GetAgentId()

    assert(agentid == ownid, "OnAgentHealthChange failed, agentid ~= ownid")
    assert(srcagentid == ownid, "OnAgentHealthChange failed, srcagentid ~= ownid")
end

function Prepare_Test_AgentMgr_OnWeaponSetSwapped()
    local agentMgr = Client:GetAgentMgr()
    local cc = Client:GetControlledCharacter()
    local tmr = Client:GetTimer()

    agentMgr:RegisterTrigger(AgentMgr.OnWeaponSetSwapped, OnWeaponSetSwapped)
    tmr:RegisterTrigger(WaitWeaponSetSwapped, 1000, 0)
    cc:SwitchWeaponSet()
end

function WaitWeaponSetSwapped()
    Test_AgentMgr_OnWeaponSetSwapped()
end

function Test_AgentMgr_OnWeaponSetSwapped()
    local agentMgr = Client:GetAgentMgr()
    local agentid = OnWeaponSetSwapped_data.agent:GetAgentId()
    local ownid = agentMgr:GetOwnId()

    assert(OnWeaponSetSwapped_data.set ~= nil)
    assert(agentid == ownid, "OnWeaponSetSwapped failed")
end

function Prepare_Test_AgentMgr_OnAgentDowned()
    local agentMgr = Client:GetAgentMgr()
    agentMgr:RegisterTrigger(AgentMgr.OnAgentDowned, OnAgentDowned)
end

function WaitKill()
    Test_AgentMgr_OnAgentDowned()
    Test_AgentMgr_OnAgentKilled()

    -- prepare the next test
    Prepare_Test_AgentMgr_OnAgentHitBySkill()
end

function Test_AgentMgr_OnAgentDowned()
    local agentMgr = Client:GetAgentMgr()
    local ownid = agentMgr:GetOwnId()

    assert(DownedAgentList, "OnAgentDowned failed: DownedAgentList is nil")

    local found = false
    for _, downed in pairs(DownedAgentList) do
        if downed:GetAgentId() == ownid then
            found = true
        end
    end

    assert(found, "OnAgentDowned failed")
end

function WaitPvPLobbyWaypoint()
    Prepare_Test_AgentMgr_OnAgentHealthChange()
    Prepare_Test_AgentMgr_OnAgentKilled2()
end

function Prepare_Test_AgentMgr_OnAgentKilled2()
    local navMgr = Client:GetNavigationMgr()
    local result = navMgr:SetTarget(FIRE_POS, 0, 0)
    assert(result, "navMgr:SetTarget failed")
    navMgr:RegisterTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Fire)
end

function Prepare_Test_AgentMgr_OnAgentKilled()
    Client:RemoveTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
    Client:RemoveTrigger(Gw2Client.OnWorldReveal, OnWorldReveal_HOTM)

    local poiMgr = Client:GetPoiMgr()
    local result = poiMgr:UseWaypoint(PVP_LOBBY_WPID)
    assert(result == PoiMgr.WaypointError.Success, "UseWaypoint failed, id=" .. PVP_LOBBY_WPID)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitPvPLobbyWaypoint, DELAY_USEWP, 0)
end

function Test_AgentMgr_OnAgentKilled()
    local agentMgr = Client:GetAgentMgr()
    local ownid = agentMgr:GetOwnId()

    assert(KilledAgentList, "OnAgentKilled failed: KilledAgentList is nil")

    local found = false
    for _, killed in pairs(KilledAgentList) do
        if killed:GetAgentId() == ownid then
            found = true
        end
    end

    assert(found, "OnAgentKilled failed")
end

function WaitDPSWaypoint()
    Prepare_Test_AgentMgr_OnAgentHitBySkill2()
end

function OnTargetReached_Golem()
    local navMgr = Client:GetNavigationMgr()
    navMgr:RemoveTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Golem)

    local cc = Client:GetControlledCharacter()
    cc:UseSkillbarSlot(GW2.SkillSlot.Weapon1)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitSkillUsed, 4000, 0)
end

function WaitSkillUsed()
    Test_AgentMgr_OnAgentHitBySkill()
end

function Test_AgentMgr_OnAgentHitBySkill()
    local agentMgr = Client:GetAgentMgr()
    local ownid = agentMgr:GetOwnId()

    assert(SkillLog, "OnAgentHitBySkill failed: SkillLog is nil")
    assert(golem, "OnAgentHitBySkill failed: golem is nil")

    agentMgr:RemoveTrigger(AgentMgr.OnAgentHitBySkill, OnAgentHitBySkill)

    local found = false
    for _, info in pairs(SkillLog) do
        -- FIXME: check the skillid
        --print("_" .. info.srcid .. "," .. info.targetid)
        if (info.srcid == ownid) and (info.targetid == golem:GetAgentId()) then
            found = true
        end
    end

    assert(found, "OnAgentHitBySkill failed")
    assert(false, "TESTS DONE")
end

function Prepare_Test_AgentMgr_OnAgentHitBySkill2()
    local navMgr = Client:GetNavigationMgr()
    local agentMgr = Client:GetAgentMgr()
    golem = agentMgr:GetClosestAgentToPoint(GOLEM_POS)
    
    navMgr:RegisterTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Golem)
    -- go to the indestructible golem
    local result = navMgr:SetTarget(GOLEM_POS, 0, 0)
    assert(result, "navMgr:SetTarget failed")
end

function Prepare_Test_AgentMgr_OnAgentHitBySkill()
    local poiMgr = Client:GetPoiMgr()
    local navMgr = Client:GetNavigationMgr()
    local agentMgr = Client:GetAgentMgr()

    local result = poiMgr:UseWaypoint(DPS_TRIALS_WPID)
    assert(result == PoiMgr.WaypointError.Success, "UseWaypoint failed, id=" .. DPS_TRIALS_WPID)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitDPSWaypoint, DELAY_USEWP, 0)
    
    agentMgr:RegisterTrigger(AgentMgr.OnAgentHitBySkill, OnAgentHitBySkill)
end
-------------------------------------------------------------------------------
-- AgentMgr events
-------------------------------------------------------------------------------
SkillLog = {}
function OnAgentHitBySkill(targetAgent, sourceAgent, skillId)
    if targetAgent == nil or sourceAgent == nil or skillId == nil then
        return
    end

    local info = {}
    info.targetid = targetAgent:GetAgentId()
    info.srcid = sourceAgent:GetAgentId()
    info.skillid = skillId

    table.insert(SkillLog, info)
end

KilledAgentList = {}
function OnAgentKilled(killedAgent, killerAgent)
    table.insert(KilledAgentList, killedAgent)
end

DownedAgentList = {}
function OnAgentDowned(downedAgent, killerAgent)
    table.insert(DownedAgentList, downedAgent)
end

OnWeaponSetSwapped_data = {}
OnWeaponSetSwapped_data.agent = nil
OnWeaponSetSwapped_data.set = nil
function OnWeaponSetSwapped(agent, set)
    OnWeaponSetSwapped_data.agent = agent
    OnWeaponSetSwapped_data.set = set
end

OnAgentSpawned_counter = 0
function OnAgentSpawned(agent)
    local mapid = Client:GetMapId()
    if (mapid == HOM_MAPID) then
        OnAgentSpawned_counter = OnAgentSpawned_counter + 1
    end
end

OnAgentDespawned_counter = 0
function OnAgentDespawned(agent)
    local mapid = Client:GetMapId()
    if (mapid == HOM_MAPID) then
        OnAgentDespawned_counter = OnAgentDespawned_counter + 1
    end
end

OnAgentHealthChange_data = {}
OnAgentHealthChange_data.agent = nil
OnAgentHealthChange_data.srcagent = nil
function OnAgentHealthChange(agent, delta, srcagent)
    OnAgentHealthChange_data.agent = agent
    OnAgentHealthChange_data.srcagent = srcagent
end

function WaitDmg()
    -- Wait a moment until fire hits us
    Test_AgentMgr_OnAgentHealthChange()

    local navMgr = Client:GetNavigationMgr()
    navMgr:RemoveTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Fire)

    Prepare_Test_AgentMgr_OnAgentDowned()
end

function OnTargetReached_Despawned()
    Test_AgentMgr_OnAgentDespawned()

    local navMgr = Client:GetNavigationMgr()
    navMgr:RemoveTrigger(NavigationMgr.OnTargetReached, OnTargetReached_Despawned)

    -- prepare next tests
    Client:RemoveTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal_HOTM)
    -- we are done here, enter pvp
    Client:EnterLeavePvP()
end

function OnTargetReached_Fire()
    local agentMgr = Client:GetAgentMgr()
    agentMgr:RegisterTrigger(AgentMgr.OnAgentKilled, OnAgentKilled)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitDmg, 2000, 0)
    tmr:RegisterTrigger(WaitKill, DELAY_FIRE_KILL, 0)
end

-------------------------------------------------------------------------------
-- AgentBase functions
-------------------------------------------------------------------------------
function Test_AgentBase_GetAgentId()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetAgent(XUNLAI_CHEST1_ID)
    assert(agent, "agentMgr:GetAgent failed")

    local agentid = agent:GetAgentId()
    assert(agentid == XUNLAI_CHEST1_ID, "agent id check failed")
end

function Test_AgentBase_GetPosition()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetAgent(XUNLAI_CHEST1_ID)
    assert(agent, "agentMgr:GetAgent failed")

    local pos = agent:GetPosition()
    assert(distance(pos, XUNLAI_CHEST1_POS) < 1.0, "Position check failed")
end

function Test_AgentBase_GetVelocity()
    -- TODO
    assert(false, "Test_AgentBase_GetVelocity")
end

function Test_AgentBase_GetType()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetOwnAgent()
    assert(agent, "agentMgr:GetAgent failed")

    type = agent:GetType()
    assert(type == AgentBase.Type.Character, "agentMgr:GetType failed")

    local agent = agentMgr:GetAgent(XUNLAI_CHEST1_ID)
    assert(agent, "agentMgr:GetAgent failed")

    type = agent:GetType()
    assert(type == AgentBase.Type.Gadget, "agentMgr:GetType failed")
end

function Test_AgentBase_GetName()
    local agentMgr = Client:GetAgentMgr()
    local agent = agentMgr:GetAgentByName(MULLENIX_NAME)
    assert(agent, "agentMgr:GetAgentByName failed")

    local name = agent:GetName()
    assert(name == MULLENIX_NAME, "agentMgr:GetName failed")
end

-------------------------------------------------------------------------------

function OnWorldReveal_HOTM()
    -- check if we are on the right map
    local mapid = Client:GetMapId()
    assert(mapid == HOTM_MAPID, "Wrong map")

    Prepare_Test_AgentMgr_OnAgentKilled()
end

function OnWorldReveal()
    -- check if we are on the right map
    local mapid = Client:GetMapId()
    if (mapid ~= HOM_MAPID) then
        print("ERROR: Wrong map. Travel to Hall of Monuments.")
        return
    end

    MaxTestDuration(120000)

    -- AgentBase functions
    Test_AgentBase_GetAgentId()
    Test_AgentBase_GetPosition()
    --Test_AgentBase_GetVelocity()
    Test_AgentBase_GetType()
    Test_AgentBase_GetName()

    -- AgentMgr functions
    Test_AgentMgr_GetAgent()
    Test_AgentMgr_GetOwnId()
    Test_AgentMgr_GetOwnAgent()
    Test_AgentMgr_GetAgentByPosition()
    Test_AgentMgr_GetAgentBySpeciesId()
    Test_AgentMgr_GetAgentByName()
    Test_AgentMgr_GetClosestAgentToPoint()
    --Test_AgentMgr_GetClosestAgentToPoint_Attitude()
    --Test_AgentMgr_GetAgentsByFilter()
    Test_AgentMgr_GetAgentCount()
    Test_AgentMgr_GetAgents()
    Test_AgentMgr_Empty()
    Test_AgentMgr_OnAgentSpawned()
    Prepare_Test_AgentMgr_OnWeaponSetSwapped()

    Prepare_Test_AgentMgr_OnAgentDespawned()
end

function init()
    print("############################################################")
    print("# API test script initialized")
    print("# This script will test most of the AgentMgr,")
    print("# AgentBase functions")
    print("# To start this script, travel to Hall of Monuments")
    print("############################################################")

    local agentMgr = Client:GetAgentMgr()
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
    agentMgr:RegisterTrigger(AgentMgr.OnAgentSpawned, OnAgentSpawned)
    agentMgr:RegisterTrigger(AgentMgr.OnAgentDespawned, OnAgentDespawned)
end