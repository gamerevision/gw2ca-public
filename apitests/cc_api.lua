--[[
    (C) Copyright 2015, Recode Systems UG & Co. KG
    All rights reserved.
 ]]
-------------------------------------------------------------------------------
-- This is a script to automatically test a few of our APIs:
-- ControlledCharacter
-- Preparation:
--   - equip Signet of Resolve (you need a guardian)
--   - start script
--   - travel to Hall of Monuments
-------------------------------------------------------------------------------

include "static_test_data.lua"
include "misc.lua"

-------------------------------------------------------------------------------
-- ControlledCharacter functions
-------------------------------------------------------------------------------
--[[
function Test_CC_GetSkillbarSkill()
    local cc = Client:GetControlledCharacter()
    -- get heal skill id
    local skillId = cc:GetSkillbarSkill(GW2.SkillSlot.Heal).skillId
    assert(skillId == SOR_SKILLID, "GetSkillbarSkill failed: skillId=" .. skillId)
end

function Prepare_Test_CC_EquipSkill()
    local tmr = Client:GetTimer()
    local cc = Client:GetControlledCharacter()
    tmr:RegisterTrigger(WaitSkillEquipped, 1000, 0)
    cc:EquipSkill(GW2.SkillSlot.Heal, SHELTER_SKILLID)
end

function WaitSkillEquipped()
    Test_CC_EquipSkill()
end

function Test_CC_EquipSkill()
    local cc = Client:GetControlledCharacter()
    -- get heal skill id
    local skillId = cc:GetSkillbarSkill(GW2.SkillSlot.Heal).skillId
    assert(skillId == SHELTER_SKILLID, "GetSkillbarSkill failed: skillId=" .. skillId)
end

function Prepare_Test_CC_UseSkillbarSlot()
    local tmr = Client:GetTimer()
    local cc = Client:GetControlledCharacter()

    cc:RegisterTrigger(ControlledCharacter.OnSkillCooldownStart, OnSkillCooldownStart)
    tmr:RegisterTrigger(WaitSkillUsed, 3500, 0)
    cc:UseSkillbarSlot(GW2.SkillSlot.Heal)
end

function WaitSkillUsed()
    Test_CC_UseSkillbarSlot()
    assert(false, "TESTS DONE")
end

function Test_CC_UseSkillbarSlot()
    assert(OnSkillCooldownStart_data.skill)

    local usedSkillId = OnSkillCooldownStart_data.skill.skillId
    assert(usedSkillId == SHELTER_SKILLID, "UseSkillbarSlot failed")
end

OnSkillCooldownStart_data = {}
OnSkillCooldownStart_data.skill = nil
function OnSkillCooldownStart(skill, cd)
    OnSkillCooldownStart_data.skill = skill
end
--]]

-------------------------------------------------------------------------------

function WaitEnter()
    Prepare_Test_CC_InvestPoints()
end

maxHP = {}
function WaitInvestPoints()
    local agentMgr = Client:GetAgentMgr()
    agent = agentMgr:GetOwnAgent()
    assert(agent, "GetOwnAgent failed")

    maxHP[0] = agent:GetMaxHealth()

    local cc = Client:GetControlledCharacter()
    -- each point in the honor traitline increase our max HP
    cc:InvestPoints(GW2.Traitline.Honor, 6)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitInvestPoints2, 2000, 0)
end

function WaitInvestPoints2()
    local agentMgr = Client:GetAgentMgr()
    agent = agentMgr:GetOwnAgent()
    assert(agent, "GetOwnAgent failed")

    maxHP[1] = agent:GetMaxHealth()

    Test_CC_InvestPoints()
    Prepare_Test_CC_SelectTrait()
end

function Test_CC_InvestPoints()
    assert(maxHP)
    assert(maxHP[0] < maxHP[1], "Test_CC_InvestPoints failed")
end

function Prepare_Test_CC_InvestPoints()
	local cc = Client:GetControlledCharacter()

	cc:InvestPoints(GW2.Traitline.Virtues, 0)
	cc:InvestPoints(GW2.Traitline.Zeal, 0)
	cc:InvestPoints(GW2.Traitline.Radiance, 0)
	cc:InvestPoints(GW2.Traitline.Valor, 0)
	cc:InvestPoints(GW2.Traitline.Honor, 0)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitInvestPoints, 2000, 0)
end

-------------------------------------------------------------------------------

function Prepare_Test_CC_SelectTrait()
    local cc = Client:GetControlledCharacter()

    -- Force of will, Vitality +300
    cc:SelectTrait(FORCE_OF_WILL, 3)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitSelectTrait, 2000, 0)
end

function WaitSelectTrait()
    local agentMgr = Client:GetAgentMgr()
    agent = agentMgr:GetOwnAgent()
    assert(agent, "GetOwnAgent failed")

    maxHP[2] = agent:GetMaxHealth()

    Test_CC_SelectTrait()
    Prepare_Test_CC_OnTraitSelected()
end

function Test_CC_SelectTrait()
    assert(maxHP[1] < maxHP[2], "Test_CC_SelectTrait failed")
end

-- TODO: remove Test_CC_Dummy_EnterPvp
function Test_CC_Dummy_EnterPvp()
    -- prepare next tests
    Client:RemoveTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal_HOTM)
    -- we are done here, enter pvp
    Client:EnterLeavePvP()
end

-------------------------------------------------------------------------------
-- ControlledCharacter events
-------------------------------------------------------------------------------
SelectTraitList = {}
function OnTraitSelected(traitId)
    table.insert(SelectTraitList, traitId)
end

function WaitOnTraitSelected()
    Test_CC_OnTraitSelected()
    Prepare_Test_CC_OnTraitDeselected()
end

function Prepare_Test_CC_OnTraitSelected()
    local cc = Client:GetControlledCharacter()
    cc:RegisterTrigger(ControlledCharacter.OnTraitSelected, OnTraitSelected)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitOnTraitSelected, 3000, 0)

    cc:SelectTrait(WRATHFUL_SPIRIT, 3)
end

function Test_CC_OnTraitSelected()
    assert(#SelectTraitList ~= 0, "OnTraitSelected failed: SelectTraitList size is 0")

    local found = false
    for _, traitId in pairs(SelectTraitList) do
        if traitId == WRATHFUL_SPIRIT then
            found = true
        end
    end

    assert(found, "OnTraitSelected failed")
end

-------------------------------------------------------------------------------
DeselectTraitList = {}
function OnTraitDeselected(traitId)
    table.insert(DeselectTraitList, traitId)
end

function WaitOnTraitDeselected()
    Test_CC_OnTraitDeselected()
    assert(false, "TESTS DONE")
end

function Prepare_Test_CC_OnTraitDeselected()
    local cc = Client:GetControlledCharacter()
    cc:RegisterTrigger(ControlledCharacter.OnTraitDeselected, OnTraitDeselected)

    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitOnTraitDeselected, 3000, 0)

    cc:InvestPoints(GW2.Traitline.Honor, 0)
end

function Test_CC_OnTraitDeselected()
    assert(#DeselectTraitList ~= 0, "OnTraitDeselected failed: DeselectTraitList size is 0")

    local found = false
    for _, traitId in pairs(DeselectTraitList) do
        if traitId == WRATHFUL_SPIRIT then
            found = true
        end
    end

    assert(found, "OnTraitDeselected failed")
end

-------------------------------------------------------------------------------

function OnWorldReveal_HOTM()
    local tmr = Client:GetTimer()
    tmr:RegisterTrigger(WaitEnter, 5000, 0)
end

function OnWorldReveal()
    -- check if we are on the right map
    local mapid = Client:GetMapId()
    if (mapid ~= HOM_MAPID) then
        error("ERROR: Wrong map. Travel to Hall of Monuments.")
        return
    end

    MaxTestDuration(30000)
--[[
	-- The skillbar stuff is currently not working
    Test_CC_GetSkillbarSkill()
    Prepare_Test_CC_EquipSkill()
    Prepare_Test_CC_UseSkillbarSlot()
--]]

    Test_CC_Dummy_EnterPvp()
end


function init()
    debug("############################################################")
    debug("# API test script initialized")
    debug("# This script will test ControlledCharacter functions")
    debug("############################################################")

    --cc:RegisterTrigger(ControlledCharacter.OnSkillCooldownStart, OnSkillCooldownStart)
    Client:RegisterTrigger(Gw2Client.OnWorldReveal, OnWorldReveal)
end