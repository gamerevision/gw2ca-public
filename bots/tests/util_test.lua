local m_util = include("gw2/util.lua")

local testTbl = {1, 2, 3}
m_util.ReverseArray(testTbl)
assert(testTbl[1] == 3 and testTbl[2] == 2 and testTbl[3] == 1,"ReverseArray() failed")

local binRepr = m_util.itoa(30, 2)
local hexRepr = m_util.itoa(30, 16)
assert(binRepr == "11110" and hexRepr == "1E", "itoa() failed")

assert(m_util.LPadString("A", 3) == "  A" and m_util.LPadString("AA", 3) == " AA", "LPadString failed")
assert(m_util.LPadString("A", 3, "+") == "++A", "LPadString failed")
assert(m_util.LPadString("AAAA", 3) == "AAAA", "LPadString failed")

assert(m_util.RPadString("A", 3) == "A  " and m_util.RPadString("AA", 3) == "AA ", "RPadString failed")
assert(m_util.RPadString("A", 3, "+") == "A++", "RPadString failed")
assert(m_util.RPadString("AAAA", 3) == "AAAA", "RPadString failed")