local money1 = Money(21)
local money2 = Money(42)
local money3 = Money(30201)

assert(money1:GetValue() == 21, "GetValue() or ctor not working")
assert(money3:GetGold() == 3,"GetGold() not working")
assert(money3:GetSilver() == 2,"GetSilver() not working")
assert(money3:GetCopper() == 1,"GetCopper() not working")

assert(money1 == money1, "op== (Money, Money) not working")
assert(money1 ~= money2, "op~= (Money, Money) not working")
assert(money1 < money2, "op< (Money, Money) not working")
assert(money2 > money1, "op> (Money, Money) not working")
assert(money1 <= money1, "op<= (Money, Money) not working")
assert(money1 <= money2, "op<= (Money, Money) not working")
assert(money2 >= money1, "op>= (Money, Money) not working")

information(money1 ~= 21)

assert(money1 == 21, "op== (Money, int) not working")
assert(money1 ~= 42, "op~= (Money, int) not working")
assert(money1 < 42, "op< (Money, int) not working")
assert(money2 > 21, "op> (Money, int) not working")
assert(money1 <= 21, "op<= (Money, int) not working")
assert(money1 <= 42, "op<= (Money, int) not working")
assert(money2 >= 21, "op>= (Money, int) not working")

assert((money1 + money1):GetValue() == 42, "op+ (Money, Money) not working")
assert((money2 - money1):GetValue() == 21, "op- (Money, Money) not working")
assert((money1 * Money(2)):GetValue() == 42, "op* (Money, Money) not working")
assert((money2 / Money(2)):GetValue() == 21, "op/ (Money, Money) not working")
assert((money1 % Money(10)):GetValue() == 1, "op% (Money, Money) not working")

assert((money1 + 21):GetValue() == 42, "op+ (Money, int) not working")
assert((money2 - 21):GetValue() == 21, "op- (Money, int) not working")
assert((money1 * 2):GetValue() == 42, "op* (Money, int) not working")
assert((money2 / 2):GetValue() == 21, "op/ (Money, int) not working")
assert((money1 % 10):GetValue() == 1, "op% (Money, int) not working")

assert((21 + money1):GetValue() == 42, "op+ (int, Money) not working")
assert((42 - money1):GetValue() == 21, "op- (int, Money) not working")
assert((2 * money1):GetValue() == 42, "op* (int, Money) not working")
assert((42 / Money(2)):GetValue() == 21, "op/ (int, Money) not working")
assert((21 % Money(10)):GetValue() == 1, "op% (int, Money) not working")

money3:SetGold(4)
assert(money3:GetValue() == 40201,"SetGold() not working")

money3:SetSilver(3)
assert(money3:GetValue() == 40301,"SetSilver() not working")

money3:SetCopper(2)
assert(money3:GetValue() == 40302,"SetCopper() not working")

